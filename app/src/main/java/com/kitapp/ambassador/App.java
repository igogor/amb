package com.kitapp.ambassador;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.hadilq.liveevent.LiveEvent;
import com.kitapp.ambassador.receivers.NetworkStateReceiver;
import com.vk.api.sdk.VK;
import com.vk.api.sdk.VKTokenExpiredHandler;

import static com.kitapp.ambassador.receivers.NetworkStateReceiver.IS_NETWORK_AVAILABLE;

public class App extends Application {
    public LiveEvent<Boolean> connStatus = new LiveEvent<>();
    private NetworkStateReceiver networkReceiver;
    @Override
    public void onCreate() {
        super.onCreate();

        //FontsOverride.setDefaultFont(this, "DEFAULT", "dinpro_bold.ttf");

        VKTokenExpiredHandler tokenTracker = () -> {

        };

        VK.addTokenExpiredHandler(tokenTracker);

        //FontsOverride.setDefaultFont(this, "DEFAULT", "DINPro-Bold.ttf");
     //   FontsOverride.setDefaultFont(this, "MONOSPACE", "MyFontAsset2.ttf");
       // FontsOverride.setDefaultFont(this, "SERIF", "MyFontAsset3.ttf");
        //FontsOverride.setDefaultFont(this, "SANS_SERIF", "MyFontAsset4.ttf");
        networkReceiver = new NetworkStateReceiver();
        registerReceiver(networkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        IntentFilter intentFilter = new IntentFilter(NetworkStateReceiver.NETWORK_AVAILABLE_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean isNetworkAvailable = intent.getBooleanExtra(IS_NETWORK_AVAILABLE, false);
                connStatus.setValue(isNetworkAvailable);
            }
        }, intentFilter);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        try {
            unregisterReceiver(networkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

}
