package com.kitapp.ambassador.viewmodels;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.google.gson.JsonObject;
import com.hadilq.liveevent.LiveEvent;
import com.kitapp.ambassador.dto.AmbassadorsBySiteDTO;
import com.kitapp.ambassador.dto.AmbassadorsDTO;
import com.kitapp.ambassador.dto.BidCustomerDTO;
import com.kitapp.ambassador.dto.CustomersDTO;
import com.kitapp.ambassador.dto.FavoriteDTO;
import com.kitapp.ambassador.dto.FilterDTO;
import com.kitapp.ambassador.dto.RewiewDTO;
import com.kitapp.ambassador.dto.modelambsite.AmbSiteFeed;
import com.kitapp.ambassador.dto.modeltask.TaskType;
import com.kitapp.ambassador.dto.net.AmbassadorsResult;
import com.kitapp.ambassador.dto.net.JwtResult;
import com.kitapp.ambassador.dto.net.SitesResult;
import com.kitapp.ambassador.dto.net.TasksResultCustomer;
import com.kitapp.ambassador.dto.net.UserProfileResult;
import com.kitapp.ambassador.fragments.task.UpdateTaskEnum;
import com.kitapp.ambassador.repo.CustomerRepository;
import com.kitapp.ambassador.repo.UserRepository;

import java.util.List;

public class CustomerViewModel extends ViewModel {
    private CustomerRepository repository = CustomerRepository.getInstance();
    private UserRepository userRepository = UserRepository.getInstance();

    public LiveEvent<JwtResult> getMessagesData() {
        return repository.getMessagesData();
    }

    public void getUserProfile(String jwt) {
        repository.getUserProfile(jwt);
    }

    public MutableLiveData<UserProfileResult> getCustomerProfileData() {
        return repository.getCustomerProfileData();
    }

    public LiveEvent<List<AmbassadorsResult>> getAmbsList(String jwt, FilterDTO dto) {
        return repository.getAmbsList(jwt, dto);
    }

    public void changeUserRole(String role, String jwt) {
        repository.changeRole(role, jwt);
    }

    public LiveEvent<String> rateAmb(RewiewDTO dto) {
        return repository.rewiewAmbs(dto);
    }

    /*public void updateUserProfile(UserProfileResult profile) {
        repository.updateUserProfile(profile);
    }
*/
    public LiveEvent<UserProfileResult> getProfileAmasadorData() {
        return repository.getAmbasadorProfilePreviewData();
    }

    public LiveEvent<List<SitesResult>> getSitesData() {
        return repository.getSitesData();
    }

    public void getSites(int position, String jwt, String search) {
        repository.getSites(position, jwt,  search);
    }

    public LiveEvent<List<TasksResultCustomer>> getTasksData() {
        return repository.getTasksData();
    }

    public MutableLiveData<List<Integer>> getFavoriteIds() {
        return userRepository.getListFavoriteIds();
    }

    public void getAmbasadorsTeams(String jwt) {
        repository.getAmbassadorTeam(jwt);
    }

    public void clientUpdateInvite(String taskid, List<Integer> userids, int status, String jwt) {
        repository.clientUpdateInvite(taskid, userids, status, jwt);
    }

    public MutableLiveData<String> getSendInvations() {
        return repository.getSendInvations();
    }

    public LiveEvent<String> clientUpdateSubtask(String subtask, String status, String jwt) {
        return repository.clientUpdateSubtask(subtask, status, jwt);
    }

    public LiveEvent<String> updateTask(String taskid, String status, String jwt) {
        return repository.updateTask(status, jwt, taskid);
    }

    public LiveEvent<String> editTask(String taskid, TaskType taskType, String jwt) {
        return repository.editTask(taskType, taskid, jwt);
    }

    public void deleteTask(String taskid, String jwt) {
        repository.deleteTask(taskid, jwt);
    }

    public LiveEvent<String> getLiveEventDelete() {
        return repository.getJwtResultLiveEventDelete();
    }

    public void getFavorites(String jwt) {
        userRepository.getFavorite(jwt);
    }

    public void getTasks(CustomersDTO data) {
        repository.getTasks(data);
    }

    public LiveEvent<List<AmbassadorsResult>> getAmbassadorsBySiteData() {
        return repository.getAmbassadorsData();
    }

    public void getAmbassadorsBySite(AmbassadorsBySiteDTO data) {
        repository.getAmbassadorsBySite(data);
    }
    public LiveEvent<List<AmbassadorsResult>> getAmbassadorsBySiteLiveEvent(AmbassadorsBySiteDTO data) {
      return  repository.getAmbassadorsBySiteLiveData(data);
    }

    public void getAmbassadorProfile(int ambassadorId, String jwt) {
        repository.getAmbassadorProfile(ambassadorId, jwt);
    }

    public LiveEvent<TaskType> getTaskData() {
        return repository.getTaskData();
    }

    public LiveEvent<TaskType> getTaskBidsData() {
        return repository.getTaskBidsData();
    }

    public LiveEvent<TaskType> getTaskInvitesData() {
        return repository.getTaskInvitesData();
    }

    public LiveEvent<TaskType> getTaskExData() {
        return repository.getTaskExData();
    }

    public LiveEvent<JsonObject> getAmbasadorsList() {
        return repository.getAmbassadorsTeam();
    }

    public void getTaskById(String taskId, String jwt) {
        repository.getTaskById(taskId, jwt);
    }

    public void getAmbsExecsTaskById(String taskId, String jwt) {
        repository.getAmbsExecsTaskById(taskId, jwt);
    }

    public void getAmbsInvitesTaskById(String taskId, String jwt) {
        repository.getAmbsInvitesTaskById(taskId, jwt);
    }

    public void getAmbsBidsTaskById(String taskId, String jwt) {
        repository.getAmbsBidsTaskById(taskId, jwt);
    }

    //Call to favorites
    public LiveEvent<JwtResult> getFavoritesData() {
        return userRepository.getResponseData();
    }

    public void favoritesAdd(FavoriteDTO data) {
        userRepository.favoritesAdd(data);
    }

    public void favoritesDelete(FavoriteDTO data) {
        userRepository.favoritesDelete(data);
    }

    public void updateBid(BidCustomerDTO data) {
        repository.updateBid(data);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        repository = null;
    }
}
