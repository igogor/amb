package com.kitapp.ambassador.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.hadilq.liveevent.LiveEvent;
import com.kitapp.ambassador.dto.AuthDTO;
import com.kitapp.ambassador.dto.RecoveryDTO;
import com.kitapp.ambassador.dto.net.BaseResult;
import com.kitapp.ambassador.dto.net.JwtResult;
import com.kitapp.ambassador.dto.net.SmsResult;
import com.kitapp.ambassador.dto.net.SmsStatusResult;
import com.kitapp.ambassador.dto.net.UserProfileResult;
import com.kitapp.ambassador.repo.AuthRepository;

public class AuthViewModel extends ViewModel {
    private AuthRepository authRepository = new AuthRepository();
    private MutableLiveData<AuthDTO> authData = new MutableLiveData<>();
    private MutableLiveData<RecoveryDTO> recoveryData = new MutableLiveData<>();

    public void checkPhone(String phoneNumber) {
        authRepository.checkPhone(phoneNumber,null);
    }

    public void sendSms(String phoneNumber) {
        authRepository.sendSms(phoneNumber);
    }

    public LiveEvent<SmsResult> getSendSms() {
        return authRepository.getSmsData();
    }

    public void login(String phoneNumber, String password) {
        authRepository.login(phoneNumber, password);
    }

    public LiveEvent<JwtResult>loginData() {
        return authRepository.getLoginData();
    }

    public void smsStatus(String phoneNumber, String smsId) {
        authRepository.smsStatus(phoneNumber, smsId);
    }

    public LiveEvent<SmsStatusResult> getSmsStatusData() {
        return authRepository.getSmsDataStatus();
    }

    public void createUser(AuthDTO auth) {
        authRepository.createUser(auth);
    }

    public void resetUserPassword(RecoveryDTO data) {
        authRepository.resetUserPassword(data);
    }

    public LiveEvent<BaseResult> getMessageData() {
        return authRepository.getMessageData();
    }

    public void validateToken(String jwt) {
        authRepository.validateToken(jwt);
    }

    public LiveEvent<UserProfileResult> getUserData() {
        return authRepository.getUserProfileData();
    }

    public MutableLiveData<AuthDTO> getAuthData() {
        return authData;
    }

    public MutableLiveData<RecoveryDTO> getRecoveryData() {
        return recoveryData;
    }

    public LiveEvent<BaseResult> getErrorData() {
        return authRepository.getErrorData();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        authRepository = null;
    }

}
