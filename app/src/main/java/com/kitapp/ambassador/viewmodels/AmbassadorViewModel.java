package com.kitapp.ambassador.viewmodels;

import androidx.lifecycle.ViewModel;

import com.hadilq.liveevent.LiveEvent;
import com.kitapp.ambassador.dto.AmbassadorsDTO;
import com.kitapp.ambassador.dto.BaseDTO;
import com.kitapp.ambassador.dto.BidDTO;
import com.kitapp.ambassador.dto.BidsFilterDTO;
import com.kitapp.ambassador.dto.ComplainDTO;
import com.kitapp.ambassador.dto.CustomerDTO;
import com.kitapp.ambassador.dto.FavoriteDTO;
import com.kitapp.ambassador.dto.RewiewDTO;
import com.kitapp.ambassador.dto.SiteDTO;
import com.kitapp.ambassador.dto.TaskDTO;
import com.kitapp.ambassador.dto.net.BaseResult;
import com.kitapp.ambassador.dto.net.BidsResult;
import com.kitapp.ambassador.dto.net.JwtResult;
import com.kitapp.ambassador.dto.net.TaskAmbassadorResult;
import com.kitapp.ambassador.dto.net.TasksResultAmbassador;
import com.kitapp.ambassador.dto.net.UserProfileResult;
import com.kitapp.ambassador.repo.AmbassadorRepository;
import com.kitapp.ambassador.repo.UserRepository;

import java.util.List;

public class AmbassadorViewModel extends ViewModel {
    private AmbassadorRepository repository = AmbassadorRepository.getInstance();
    private UserRepository userRepository = UserRepository.getInstance();

    public void getSite(String url, String jwt) {
        repository.getSite(url, jwt);
    }

    public LiveEvent<SiteDTO> getSiteData() {
        return repository.getSiteData();
    }

    public LiveEvent<Boolean> updateSite(SiteDTO site) {
        return repository.updateSite(site);
    }

    public LiveEvent<JwtResult> getUpdateSiteData() {
        return repository.getUpdateSiteData();
    }

    public LiveEvent<String> rateClient(RewiewDTO data){
        return repository.rateClient(data);
    }
   public LiveEvent<String> complainAboutCustomer(ComplainDTO data){
        return repository.complainAboutCustomer(data);
    }

    public LiveEvent<BaseResult> getMessageData() {
        return repository.getMessageData();
    }

    public LiveEvent<UserProfileResult> getUserProfile(String jwt) {
        return repository.getUserProfile(jwt);
    }

    public LiveEvent<UserProfileResult> getUserProfileData() {
        return repository.getUserProfileData();
    }

    public LiveEvent<UserProfileResult> getProfilePreviewData() {
        return repository.getUserProfilePreviewData();
    }

    public void changeUserRole(String role, String jwt) {
        repository.changeRole(role, jwt);
    }

    public void updateUserProfile(UserProfileResult profile) {
        repository.updateUserProfile(profile);
    }

    public void updateUserPhoto(String photo, String jwt) {
        repository.updatePhoto(photo, jwt);
    }

    public void getAmbassadorsTasks(AmbassadorsDTO data) {
        repository.getAmbassadorsTask(data);
    }

    public LiveEvent<List<TasksResultAmbassador>> getAmbassadorTasksData() {
        return repository.getAmbassadorsTasksData();
    }

    public LiveEvent<List<BidsResult>> getBidsData() {
        return repository.getBidsData();
    }

    public void getBids(BidsFilterDTO data) {
        repository.getBids(data);
    }

    public LiveEvent<String> updateBid(BidDTO data) {
        return repository.updateBid(data);
    }

    public LiveEvent<TaskAmbassadorResult> getTaskData() {
        return repository.getTaskData();
    }

    public void getTaskById(TaskDTO task) {
        repository.getTask(task);
    }

    //Call to favorites
    public LiveEvent<JwtResult> getFavoritesData() {
        return userRepository.getResponseData();
    }

    public void favoritesAdd(FavoriteDTO data) {
        userRepository.favoritesAdd(data);
    }

    public void favoritesDelete(FavoriteDTO data) {
        userRepository.favoritesDelete(data);
    }

    public LiveEvent<String> updateInvite(BidDTO data) {
        return repository.updateInvite(data);
    }

    public LiveEvent<JwtResult> doneTask(BidDTO data) {
        return repository.doneTask(data);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        repository = null;
    }

    public void getClientProfile(CustomerDTO data) {
        repository.getClientProfile(data);
    }
}
