package com.kitapp.ambassador;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import com.kitapp.ambassador.helpers.Constants;
import com.kitapp.ambassador.viewmodels.AuthViewModel;

public class SplashScreenActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
    }

    @Override
    protected void doCall() {

        if (!jwt.equals("")) {
            AuthViewModel authViewModel = ViewModelProviders.of(this).get(AuthViewModel.class);
            authViewModel.getMessageData().observe(this, message -> {
                startActivity(new Intent(this, LoginActivity.class));
                finish();
            });
            authViewModel.getUserData().observe(this, result -> {
                if (result.getRole().equals(Constants.ROLE_AMBASSADOR)) {
                    startActivity(new Intent(this, MainActivity.class));
                } else {
                    startActivity(new Intent(this, CustomerActivity.class));
                }
                finish();
            });
            authViewModel.validateToken(jwt);

        } else {
            new Handler().postDelayed(() -> {
                Intent intent = new Intent(getApplicationContext(), IntroActivity.class);
                startActivity(intent);
                finish();
            }, 2000);
        }
    }
}
