package com.kitapp.ambassador;

import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.kitapp.ambassador.dialogs.TaskDialogMessage;
import com.kitapp.ambassador.helpers.Constants;
import com.kitapp.ambassador.helpers.SharedPrefHelper;

public class BaseActivity extends AppCompatActivity {
    protected String jwt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        jwt = SharedPrefHelper.getString(this, Constants.JWT_PREFERENCES, "");

        ((App)getApplication()).connStatus.observe(this, status -> {
            if (status)
                doCall();
            else
                showErrorDialog();
        });
    }

    protected void showErrorDialog() {
        TaskDialogMessage dialog = TaskDialogMessage.newInstance("Ошибка", "Интернет соединение отсутствует");
        dialog.show(getSupportFragmentManager(), "Dialog Error Connection");
    }

    @Override
    public boolean dispatchTouchEvent(@NonNull MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                v.clearFocus();
                InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }

        return super.dispatchTouchEvent(event);
    }

    protected void doCall() {

    }
}
