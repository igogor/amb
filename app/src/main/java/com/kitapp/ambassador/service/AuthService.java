package com.kitapp.ambassador.service;

import android.util.ArrayMap;

import com.kitapp.ambassador.dto.net.BaseResult;
import com.kitapp.ambassador.dto.net.JwtResult;
import com.kitapp.ambassador.dto.net.SmsResult;
import com.kitapp.ambassador.dto.net.SmsStatusResult;
import com.kitapp.ambassador.dto.net.UserProfileResult;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AuthService {
    @POST("api/sms_send.php")
    Call<SmsResult> sendSms(@Body ArrayMap<String, Object> body); //TODO replace ArrayMap by DTO

    @POST("api/login.php")
    Call<JwtResult> login(@Body ArrayMap<String, Object> body);

    @POST("api/validate_token.php")
    Call<UserProfileResult> validateToken(@Body ArrayMap<String, Object> body);

    @POST("api/sms_status.php")
    Call<SmsStatusResult> smsStatus(@Body ArrayMap<String, Object> body);

    @POST("api/create_user.php")
    Call<BaseResult> createUser(@Body ArrayMap<String, Object> body);

    @POST("api/reset_user_password.php")
    Call<BaseResult> resetUserPassword(@Body ArrayMap<String, Object> body);

    @POST("api/user_exists.php")
    Call<BaseResult> checkUser(@Body ArrayMap<String, Object> body);

}
