package com.kitapp.ambassador.service;

import com.kitapp.ambassador.dto.BaseDTO;
import com.kitapp.ambassador.dto.FavoriteDTO;
import com.kitapp.ambassador.dto.net.JwtResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserService {

    @POST("api/favorites_add.php")
    Call<JwtResult> favoritesAdd(@Body FavoriteDTO body);

    @POST("api/favorites_delete.php")
    Call<JwtResult> favoritesDelete(@Body FavoriteDTO body);


    @POST("api/favorites_list.php")
    Call<List<Integer>> getFavorites(@Body BaseDTO body);
}
