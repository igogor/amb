package com.kitapp.ambassador.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceBuilder {
    private static final String URL = "https://";

    private static HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor(message -> Log.i("Interceptor", message))
                .setLevel(HttpLoggingInterceptor.Level.BODY);

    private static OkHttpClient okHttp = new OkHttpClient.Builder()
            //.addInterceptor(new HeaderInterceptor())
            .addInterceptor(logInterceptor)
            .build();
    private static Gson gson = new GsonBuilder()
            .setLenient()
            .create();
    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(URL)
            .client(okHttp)
            .addConverterFactory(GsonConverterFactory.create(gson));

    private static Retrofit retrofit = builder.build();

    public static <S> S buildService(Class<S> serviceType) { return retrofit.create(serviceType);
    }
}
