package com.kitapp.ambassador.helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefHelper {
    private final static String PREF_FILE = Constants.PREFERENCE_NAME;

    public static void setString(Context context, String key, String value){
        SharedPreferences settings = context.getSharedPreferences(context.getPackageName() + PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getString(Context context, String key, String defValue){
        SharedPreferences settings = context.getSharedPreferences(context.getPackageName() + PREF_FILE, Context.MODE_PRIVATE);
        return settings.getString(key, defValue);
    }

    public static void setInt(Context context, String key, int value){
        SharedPreferences settings = context.getSharedPreferences(context.getPackageName() + PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static int getInt(Context context, String key, int defValue){
        SharedPreferences settings = context.getSharedPreferences(context.getPackageName() + PREF_FILE, Context.MODE_PRIVATE);
        return settings.getInt(key, defValue);
    }

    public static void setBoolean(Context context, String key, boolean value){
        SharedPreferences settings = context.getSharedPreferences(context.getPackageName() + PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static boolean getBoolean(Context context, String key, boolean defValue){
        SharedPreferences settings = context.getSharedPreferences(context.getPackageName() + PREF_FILE, Context.MODE_PRIVATE);
        return settings.getBoolean(key, defValue);
    }
}

