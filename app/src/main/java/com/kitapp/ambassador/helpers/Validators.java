package com.kitapp.ambassador.helpers;

import android.content.Context;
import android.text.TextUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validators {
    private static final String PHONE_PATTERN = "^\\+(38|7)(\\()?([0-9]{3})(\\)|-)?([0-9]{3})(-)?([0-9]{2}(-)?[0-9]{2})$";
    private static final String EMAIL_PATTERN = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
    private static final String PASSWORD_PATTERN = "^[a-z0-9_-]{8,25}$";

    public static boolean validatePhoneNumber(String phoneNumber, EditText view) {
        boolean valid = true;

        if (!phoneNumber.matches(PHONE_PATTERN)) {
            view.setError("Неправильный номер телефона");
            valid = false;
        } else {
            view.setError(null);
        }
        return valid;
    }

    public static boolean validatePassword(String password, EditText view) {
        boolean valid = true;

        if (TextUtils.isEmpty(password) || password.length() < 6) {
            view.setError(Constants.PASSWORD_MUST_CONTAIN_AT_LEAST_6_CHARACTERS);
            valid = false;
        } else if (!password.matches(PASSWORD_PATTERN)) {
            view.setError(Constants.PASSWORD_MUST_CONTAIN_ONLY_ALPHANUMERIC_CHARACTERS);
            valid = false;
        } else {
            view.setError(null);
        }
        return valid;
    }

    public static boolean validateEmail(String email, EditText view) {
        boolean valid = true;
        Matcher matcher = Pattern.compile(EMAIL_PATTERN, Pattern.CASE_INSENSITIVE).matcher(email);

        if (!matcher.find()) {
            view.setError(Constants.INVALID_EMAIL_FORMAT);
            valid = false;
        } else {
            view.setError(null);
        }
        return valid;
    }

    public static boolean isNotEmpty(String value, EditText view) {
        boolean valid = true;

        if (TextUtils.isEmpty(value)) {
            view.setError("Заполните поле");
            valid = false;
        } else {
            view.setError(null);
        }
        return valid;
    }

    public static boolean isChecked(Context context, CheckBox view) {
        boolean valid = true;

        if (!view.isChecked()) {
            Toast.makeText(context, "Примите условия соглашения", Toast.LENGTH_SHORT).show();
            valid = false;
        }
        return valid;
    }

}
