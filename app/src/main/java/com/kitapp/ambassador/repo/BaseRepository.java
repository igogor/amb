package com.kitapp.ambassador.repo;

import com.hadilq.liveevent.LiveEvent;
import com.kitapp.ambassador.dto.net.BaseResult;

import java.io.IOException;

import okhttp3.ResponseBody;

class BaseRepository {
    private LiveEvent<BaseResult> errorData = new LiveEvent<>();

    protected <T extends BaseResult> T setError(ResponseBody error, T result) {
        try {
            result.setMessage(error.string());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    protected void showErrorMessage(Throwable t) {
        errorData.postValue(new BaseResult("Response Error: " + t.getMessage()));
    }

    public LiveEvent<BaseResult> getErrorData() {
        return errorData;
    }
}
