package com.kitapp.ambassador.repo;

import android.util.ArrayMap;

import androidx.annotation.NonNull;

import com.hadilq.liveevent.LiveEvent;
import com.kitapp.ambassador.dto.AuthDTO;
import com.kitapp.ambassador.dto.RecoveryDTO;
import com.kitapp.ambassador.dto.net.BaseResult;
import com.kitapp.ambassador.dto.net.JwtResult;
import com.kitapp.ambassador.dto.net.SmsResult;
import com.kitapp.ambassador.dto.net.SmsStatusResult;
import com.kitapp.ambassador.dto.net.UserProfileResult;
import com.kitapp.ambassador.helpers.Constants;
import com.kitapp.ambassador.service.AuthService;
import com.kitapp.ambassador.service.ServiceBuilder;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthRepository extends BaseRepository {

    private AuthService authService;
    private LiveEvent<BaseResult> messageData = new LiveEvent<>();
    private LiveEvent<SmsResult> smsData = new LiveEvent<>();
    private LiveEvent<SmsStatusResult> smsDataStatus = new LiveEvent<>();
    private LiveEvent<JwtResult> loginData = new LiveEvent<>();
    private LiveEvent<UserProfileResult> profileData = new LiveEvent<>();

    public AuthRepository() {
        authService = ServiceBuilder.buildService(AuthService.class);
    }


    public LiveEvent<SmsResult> getSmsData() {
        return smsData;
    }

    public void sendSms(String phoneNumber) {
        ArrayMap<String, Object> map = new ArrayMap<>();
        map.put("phone", phoneNumber);
        authService.sendSms(map)
                .enqueue(new Callback<SmsResult>() {
                    @Override
                    public void onResponse(@NonNull Call<SmsResult> call, @NonNull Response<SmsResult> response) {
                        if (response.isSuccessful()) {
                            SmsResult result = response.body();
                            if (result.getCode() == null) {
                                messageData.postValue(new BaseResult(result.getError()));
                            } else
                                smsData.postValue(result);
                        }
                        else
                            messageData.postValue(setError(response.errorBody(), new BaseResult()));
                    }

                    @Override
                    public void onFailure(@NonNull Call<SmsResult> call, @NonNull Throwable t) {
                        showErrorMessage(t);
                    }
                });
    }

    public LiveEvent<JwtResult> getLoginData() {
        return loginData;
    }

    public void login(String phoneNumber, String password) {
        ArrayMap<String, Object> map = new ArrayMap<>();
        map.put("phone", phoneNumber);
        map.put("password", password);
        authService.login(map)
                .enqueue(new Callback<JwtResult>() {
                    @Override
                    public void onResponse(@NonNull Call<JwtResult> call, @NonNull Response<JwtResult> response) {
                        if (response.isSuccessful()) {
                            JwtResult result = response.body();
                            loginData.postValue(result);
                        } else
                            messageData.postValue(setError(response.errorBody(), new BaseResult()));
                    }

                    @Override
                    public void onFailure(@NonNull Call<JwtResult> call, @NonNull Throwable t) {
                        showErrorMessage(t);
                    }
                });
    }

    public LiveEvent<UserProfileResult> getUserProfileData() {
        return profileData;
    }

    public void validateToken(String jwt) {
        ArrayMap<String, Object> map = new ArrayMap<>();
        map.put("jwt", jwt);
        authService.validateToken(map)
                .enqueue(new Callback<UserProfileResult>() {
                    @Override
                    public void onResponse(@NonNull Call<UserProfileResult> call, @NonNull Response<UserProfileResult> response) {
                        if (response.isSuccessful())
                            profileData.postValue(response.body());
                        else {
                            try {
                                String body = response.errorBody().string();
                                if (body.contains("Access denied."))
                                    messageData.postValue(new BaseResult("Access denied."));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<UserProfileResult> call, @NonNull Throwable t) {
                        showErrorMessage(t);
                    }
                });
    }

    public LiveEvent<SmsStatusResult> getSmsDataStatus() {
        return smsDataStatus;
    }

    public void smsStatus(String phone, String smsId) {
        ArrayMap<String, Object> map = new ArrayMap<>();
        map.put("phone", phone);
        map.put("smsid", smsId);
        authService.smsStatus(map)
                .enqueue(new Callback<SmsStatusResult>() {
                    @Override
                    public void onResponse(@NonNull Call<SmsStatusResult> call, @NonNull Response<SmsStatusResult> response) {
                        if (response.isSuccessful()) {
                            SmsStatusResult result = response.body();
                            if (result.getStatus().equals(Constants.VALID_SMS_STATUS))
                                smsDataStatus.postValue(result);
                            else
                                messageData.postValue(new BaseResult(result.getError()));
                        }
                        else {
                            messageData.postValue(setError(response.errorBody(), new BaseResult()));
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<SmsStatusResult> call, @NonNull Throwable t) {
                        showErrorMessage(t);
                    }
                });
    }


    public LiveEvent<BaseResult> getMessageData() {
        return messageData;
    }

    public void createUser(AuthDTO auth) {
        ArrayMap<String, Object> map = new ArrayMap<>();
        map.put("firstname", auth.getName());
        map.put("lastname", auth.getSurname());
        map.put("password", auth.getPassword());
        map.put("phone", auth.getPhoneNumber());
        map.put("email", auth.getEmail());
        map.put("role", auth.getRole());
        map.put("birthday", auth.getBirthday());
        map.put("city", auth.getCity());
        map.put("gender", auth.getGender());
        map.put("imei", auth.getImei());
        map.put("lat", auth.getLat());
        map.put("lng", auth.getLng());
        authService.createUser(map)
                .enqueue(new Callback<BaseResult>() {
                    @Override
                    public void onResponse(@NonNull Call<BaseResult> call, @NonNull Response<BaseResult> response) {
                        if (response.isSuccessful()) {
                            messageData.postValue(response.body());
                        }
                        else
                            messageData.postValue(setError(response.errorBody(), new BaseResult()));
                    }

                    @Override
                    public void onFailure(@NonNull Call<BaseResult> call, @NonNull Throwable t) {
                        showErrorMessage(t);
                    }
                });
    }

    public void resetUserPassword(RecoveryDTO data) {
        ArrayMap<String, Object> map = new ArrayMap<>();
        map.put("phone", data.getPhone());
        if (!data.getSmsCode().isEmpty()) {
            map.put("code", data.getSmsCode());
            map.put("newpassword", data.getPassword());
        }
        authService.resetUserPassword(map)
                .enqueue(new Callback<BaseResult>() {
                    @Override
                    public void onResponse(@NonNull Call<BaseResult> call, @NonNull Response<BaseResult> response) {
                        if (response.isSuccessful())
                            messageData.postValue(response.body());
                        else
                            messageData.postValue(setError(response.errorBody(), new BaseResult()));
                    }

                    @Override
                    public void onFailure(@NonNull Call<BaseResult> call, @NonNull Throwable t) {
                        showErrorMessage(t);
                    }
                });
    }

    public void checkPhone(String phone, String email) {
        ArrayMap<String, Object> map = new ArrayMap<>();
        if ((phone != null) && !phone.isEmpty())
            map.put("phone", phone);
        if ((email != null) && !email.isEmpty()) {
            map.put("email", email);
        }
        authService.checkUser(map)
                .enqueue(new Callback<BaseResult>() {
                    @Override
                    public void onResponse(@NonNull Call<BaseResult> call, @NonNull Response<BaseResult> response) {
                        if (response.isSuccessful())
                            messageData.postValue(new BaseResult("Номер уже зарегистрирован"));
                        else {
                            if ((phone != null) && !phone.isEmpty())
                                sendSms(phone);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<BaseResult> call, @NonNull Throwable t) {
                        showErrorMessage(t);
                    }
                });
    }


}
