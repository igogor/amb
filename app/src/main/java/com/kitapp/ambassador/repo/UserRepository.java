package com.kitapp.ambassador.repo;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.hadilq.liveevent.LiveEvent;
import com.kitapp.ambassador.dto.BaseDTO;
import com.kitapp.ambassador.dto.FavoriteDTO;
import com.kitapp.ambassador.dto.net.JwtResult;
import com.kitapp.ambassador.service.ServiceBuilder;
import com.kitapp.ambassador.service.UserService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRepository extends BaseRepository {

    private static UserRepository INSTANCE;

    public static UserRepository getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new UserRepository();
        }
        return INSTANCE;
    }

    private UserService userservice;
    private LiveEvent<JwtResult> jwtData = new LiveEvent<>();
    private MutableLiveData<List<Integer>> listIdsData = new MutableLiveData<>();

    private UserRepository() {
        userservice = ServiceBuilder.buildService(UserService.class);
    }

    public LiveEvent<JwtResult> getResponseData() {
        return jwtData;
    }

    public void favoritesAdd(FavoriteDTO data) {
        userservice.favoritesAdd(data)
                .enqueue(new Callback<JwtResult>() {
                    @Override
                    public void onResponse(Call<JwtResult> call, Response<JwtResult> response) {
                        if (response.isSuccessful())
                            jwtData.postValue(response.body());
                        else
                            jwtData.postValue(setError(response.errorBody(), new JwtResult()));
                    }

                    @Override
                    public void onFailure(Call<JwtResult> call, Throwable t) {

                    }
                });
    }

    public void favoritesDelete(FavoriteDTO data) {
        userservice.favoritesDelete(data)
                .enqueue(new Callback<JwtResult>() {
                    @Override
                    public void onResponse(Call<JwtResult> call, Response<JwtResult> response) {
                        if (response.isSuccessful())
                            jwtData.postValue(response.body());
                        else
                            jwtData.postValue(setError(response.errorBody(), new JwtResult()));
                    }

                    @Override
                    public void onFailure(Call<JwtResult> call, Throwable t) {

                    }
                });
    }

    public void getFavorite(String jwt) {
        userservice.getFavorites(new BaseDTO((jwt)))
                .enqueue(new Callback<List<Integer>>() {
                    @Override
                    public void onResponse(Call<List<Integer>> call, Response<List<Integer>> response) {
                        listIdsData.postValue(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<Integer>> call, Throwable t) {
                    }
                });
    }

    public MutableLiveData<List<Integer>> getListFavoriteIds() {
        return listIdsData;
    }
}
