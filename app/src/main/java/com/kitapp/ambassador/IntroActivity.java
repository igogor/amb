package com.kitapp.ambassador;

import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.kitapp.ambassador.dto.IntroDTO;
import com.kitapp.ambassador.fragments.IntroFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IntroActivity extends AppCompatActivity implements IntroFragment.OnButtonClick {
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.intro_toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbarText)
    TextView toolbarText;

    private int numScreens = 4;
    private int currentIndex = 0;
    private TypedArray images;
    private String[] titles;
    private String[] info;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_viewpager);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toolbarText.setText(R.string.intro_screen_title);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);

        Resources res = getResources();
        images = res.obtainTypedArray(R.array.intro_image_id_array);
        titles = res.getStringArray(R.array.intro_title_array);
        info = res.getStringArray(R.array.intro_info_array);

        FragmentManager fragmentManager = getSupportFragmentManager();
        viewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                IntroDTO intro = new IntroDTO(images.getResourceId(position, 0), titles[position], info[position]);
                return IntroFragment.newInstance(intro, position);
            }

            @Override
            public int getCount() {
                return numScreens;
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        images.recycle();
    }

    @Override
    public void onSkipClicked() {
        goLoginActivity();
    }

    @Override
    public void onNextClicked(int index) {
        currentIndex = index;
        if (currentIndex == numScreens) {
            goLoginActivity();
        } else {
            viewPager.setCurrentItem(currentIndex);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void goLoginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                viewPager.setCurrentItem(--currentIndex);
                if (currentIndex == 0)
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                break;
        }
        return true;
    }
}
