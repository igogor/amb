package com.kitapp.ambassador.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.kitapp.ambassador.dto.SmsHandler;

import java.sql.Timestamp;
import java.util.Date;

public class SmsReceiver extends BroadcastReceiver {

    public static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

    private String smsMessage, smsPhone = "";
    private SmsHandler smsHandler;

    public SmsReceiver(SmsHandler smsHandler) {
        this.smsHandler = smsHandler;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() == SMS_RECEIVED) {
            Bundle bundle = intent.getExtras();
            if (bundle!= null) {
                Object[] mypdu = (Object[]) bundle.get("pdus");
                final SmsMessage[] message = new SmsMessage[mypdu.length];

                for (int i = 0; i < mypdu.length; i++) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        String format = bundle.getString("format");
                        message[i] = SmsMessage.createFromPdu((byte[]) mypdu[i], format);
                    } else {
                        message[i] = SmsMessage.createFromPdu((byte[]) mypdu[i]);
                    }

                    String messageBody = message[i].getMessageBody();
                    smsPhone = message[i].getOriginatingAddress();

                    if (messageBody.contains("PR Ambassador")) {
                        smsMessage = messageBody.split(" - ")[1];
                        Timestamp ts = new Timestamp(message[i].getTimestampMillis());
                        Date date = new Date(ts.getTime());
                        smsHandler.handleSms(smsPhone, smsMessage, date);
                    }
                }
            }
        }
    }
}
