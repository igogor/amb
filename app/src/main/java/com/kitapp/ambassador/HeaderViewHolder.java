package com.kitapp.ambassador;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

class HeaderViewHolder {
    @BindView(R.id.menuPhoto)
    ImageView photoView;
    @BindView(R.id.menuHumb)
    ImageView humbView;
    @BindView(R.id.menuEmail)
    TextView emailView;

    HeaderViewHolder(View view) {
        ButterKnife.bind(this, view);
    }
}
