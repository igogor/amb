package com.kitapp.ambassador.fragments.login;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;

import com.edittextpicker.aliazaz.EditTextPicker;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.kitapp.ambassador.BuildConfig;
import com.kitapp.ambassador.R;
import com.kitapp.ambassador.dto.AuthDTO;
import com.kitapp.ambassador.dto.net.SmsResult;
import com.kitapp.ambassador.dto.net.UserVKResult;
import com.kitapp.ambassador.helpers.Constants;
import com.kitapp.ambassador.helpers.Validators;
import com.kitapp.ambassador.repo.YandexRepository;
import com.kitapp.ambassador.service.VKUserRequest;
import com.vk.api.sdk.VK;
import com.vk.api.sdk.VKApiCallback;
import com.vk.api.sdk.auth.VKScope;
import com.vk.api.sdk.exceptions.VKApiExecutionException;
import com.vk.api.sdk.utils.VKUtils;
import com.yandex.authsdk.YandexAuthException;
import com.yandex.authsdk.YandexAuthOptions;
import com.yandex.authsdk.YandexAuthSdk;
import com.yandex.authsdk.YandexAuthToken;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AuthFragment extends BaseAuthFragment {
    private static final String TAG = AuthFragment.class.getName();
    private static final int GOOGLE_SIGN_IN_CODE = 101;
    private static final int FACEBOOK_SIGN_IN_CODE = 64206;
    private static final int YANDEX_SIGN_IN_CODE = 777;
    public static final int VK_SIGN_IN_CODE = 282;
    public static final String VK_USER_ID = "userID";

    @BindView(R.id.authPhone)
    EditTextPicker phoneText;
    @BindView(R.id.authSpinner)
    Spinner codeCountryText;
    @BindView(R.id.hintText)
    TextView hintText;

    private AuthClicked listener;
    private AuthDTO auth = new AuthDTO();
    private String[] codes;
    private CallbackManager callbackManager;
    private YandexAuthSdk yandexAuthSdk;
    private GoogleSignInOptions gso;
    private Observer<SmsResult> observer;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AuthClicked) {
            listener = (AuthClicked) context;
        } else {
            throw new ClassCastException("$context must implement AuthClicked interface");
        }
    }

    @SuppressLint("LongLogTag")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        yandexAuthSdk = new YandexAuthSdk(getContext(), new YandexAuthOptions(getContext(), true));

        VK.initialize(getApplicationContext());
        String[] fingerprints = VKUtils.getCertificateFingerprint(getActivity(), this.getActivity().getPackageName());
        View view = inflater.inflate(R.layout.activity_login_auth, container, false);
        ButterKnife.bind(this, view);

        observer = smsResult -> {
            auth.setSmsId(smsResult.getId());
            auth.setSmsCode(smsResult.getCode());
            AuthFragment.this.saveAuth();
            listener.onButtonClick(Constants.LOGIN_SMS_FRAGMENT);
            if (BuildConfig.DEBUG)
                Toast.makeText(AuthFragment.this.getActivity(), "Код SMS - " + smsResult.getCode(), Toast.LENGTH_SHORT).show();
            viewModel.getSendSms().removeObserver(observer);
        };

        hintText.setOnClickListener(v -> new SimpleTooltip.Builder(getActivity())
                .anchorView(v)
                .text("Tooltip text")
                .gravity(Gravity.END)
                .build().show());

        codes = getResources().getStringArray(R.array.login_code_region_array);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_textview, codes);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        codeCountryText.setAdapter(adapter);

        return view;
    }

    /*@Override
    public void onStart() {
        super.onStart();
        GoogleSignInAccount alreadyloggedAccount = GoogleSignIn.getLastSignedInAccount(getActivity());
        if (alreadyloggedAccount != null) {
            Toast.makeText(getActivity(), "Already Logged In", Toast.LENGTH_SHORT).show();
            onLoggedIn(alreadyloggedAccount);
        } else {
            Log.d(TAG, "Not logged in");
        }
    }*/

    @Override
    public void onStop() {
        super.onStop();
        LoginManager.getInstance().logOut();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
            switch (requestCode) {
                case GOOGLE_SIGN_IN_CODE:
                    try {
                        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                        GoogleSignInAccount account = task.getResult(ApiException.class);

                        setAuth(account.getGivenName(), account.getFamilyName(), account.getEmail(), "", "");
                        onLoggedIn();
                    } catch (ApiException e) {
                        Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
                    }
                    break;
                case FACEBOOK_SIGN_IN_CODE:
                    callbackManager.onActivityResult(requestCode, resultCode, data);
                    break;
                case YANDEX_SIGN_IN_CODE:
                    try {
                        final YandexAuthToken authToken = yandexAuthSdk.extractToken(resultCode, data);
                        if (authToken != null) {
                            YandexRepository.getInstance().getUserInfo(authToken.getValue()).observe(this, yandexResult -> {

                                setAuth(yandexResult.getFirstName(), yandexResult.getLastName(), yandexResult.getDefaultEmail(), "", "");
                                onLoggedIn();
                            });
                            break;
                        }
                    } catch (YandexAuthException e) {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                case VK_SIGN_IN_CODE:
                    int id = data.getIntExtra(VK_USER_ID, 0);
                    VK.execute(new VKUserRequest(id), new VKApiCallback<UserVKResult>() {
                        @Override
                        public void success(UserVKResult user) {
                            String birthday = user.getBirthdate().replaceAll("[/\\./]", "/");
                            String gender = user.getSex().equals("1") ? "Женский" : "Мужской";

                            setAuth(user.getFirstName(), user.getLastName(), "", birthday, gender);
                            onLoggedIn();
                        }

                        @Override
                        public void fail(@NotNull VKApiExecutionException e) {
                            onLoggedIn();
                        }
                    });
                    break;
            }
    }

    private void setAuth(String name, String surname, String email, String birthday, String sex) {
        auth.setName(name);
        auth.setSurname(surname);
        auth.setEmail(email);
        auth.setBirthday(birthday);
        auth.setGender(sex);
        saveAuth();
    }

    private void onLoggedIn() {
        listener.onButtonClick(Constants.LOGIN_ROLE_FRAGMENT);
    }

    private void onLoggedIn(GoogleSignInAccount googleSignInAccount) {
        /*Intent intent = new Intent(this, ProfileContentFragment.class);
        intent.putExtra(ProfileContentFragment.GOOGLE_ACCOUNT, googleSignInAccount);

        startActivity(intent);
        finish();*/
        Toast.makeText(getActivity(), googleSignInAccount.getEmail() + " SIGNED IN", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.authRegister)
    void onRegisterClick() {

        String inputPhone = codeCountryText.getSelectedItem().toString() + getInputPhone();
        if (Validators.validatePhoneNumber(inputPhone, phoneText)) {
            String phoneNoMask = inputPhone.replaceAll("[^+0-9]", "");

            auth.setPhoneNumber(phoneNoMask);
            auth.setPhoneNumberMask(inputPhone);
            viewModel.getSendSms().observe(getViewLifecycleOwner(), observer);
            // viewModel.sendSms(phoneNoMask);
            viewModel.checkPhone(phoneNoMask);
        }
    }

    @NotNull
    private String getInputPhone() {
        return phoneText.getText().toString().trim();
    }

    private void saveAuth() {
        viewModel.getAuthData().postValue(auth);
    }

    @OnClick(R.id.authSignin)
    void onSigninClick() {
        saveAuth();
        listener.onButtonClick(Constants.LOGIN_SIGNIN_FRAGMENT);
    }

    @OnClick(R.id.facebookButton)
    void onFacebookButtonClicked() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
        //LoginManager.getInstance().logOut();
        /*if (isLoggedIn) {
            Profile profile = Profile.getCurrentProfile();
            if (profile != null) {
                LoginManager.getInstance().logOut();
            }
        }
        else*/
        setupFacebook();
    }

    private ProfileTracker mProfileTracker;

    private void setupFacebook() {
        LoginManager loginManager = LoginManager.getInstance();
        callbackManager = CallbackManager.Factory.create();

        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (Profile.getCurrentProfile() == null) {
                    mProfileTracker = new ProfileTracker() {
                        @Override
                        protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                            Log.v("facebook - profile", currentProfile.getFirstName());
                            mProfileTracker.stopTracking();
                            setAuth(currentProfile.getFirstName(), currentProfile.getLastName(), "", "", "");
                            auth.setAuthViaFacebook(true);
                            onLoggedIn();
                        }
                    };
                    mProfileTracker.startTracking();
                    // no need to call startTracking() on mProfileTracker
                    // because it is called by its constructor, internally.
                } else {
                    Profile profile = Profile.getCurrentProfile();
                    setAuth(profile.getFirstName(), profile.getLastName(), "", "", "");
                    auth.setAuthViaFacebook(true);
                    onLoggedIn();
                }
            }

            @Override
            public void onCancel() {
                onLoggedIn();
                Toast.makeText(getActivity(), "Cancel login", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        loginManager.logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));//user_age_range, user_birthday, user_gender, user_hometown, user_location
    }

    @OnClick(R.id.yandexButton)
    void onYandexButtonClicked() {
        Set<String> set = new HashSet<>(Arrays.asList("profile", "email'"));
        startActivityForResult(yandexAuthSdk.createLoginIntent(getActivity(), set), YANDEX_SIGN_IN_CODE);
    }

    @OnClick(R.id.googleButton)
    void onGoogleButtonClicked() {
        if (gso == null) {
            gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestServerAuthCode(getString(R.string.google_client_id))
                    .requestEmail()
                    .requestProfile()
                    .build();
        }
/*
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(getActivity())
                // .addConnectionCallbacks(this)
                //.addOnConnectionFailedListener(this)
                .enableAutoManage(getActivity(), this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();*/

        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, GOOGLE_SIGN_IN_CODE);
        /*Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, 101);*/
    }

    @OnClick(R.id.vkButton)
    void onVkButtonClicked() {
        VK.login(getActivity(), Arrays.asList(VKScope.EMAIL, VKScope.STATS));
    }
}
