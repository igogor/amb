package com.kitapp.ambassador.fragments.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kitapp.ambassador.MainActivity;
import com.kitapp.ambassador.R;
import com.kitapp.ambassador.dto.SiteDTO;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class YandexFragment extends BaseSiteFragment {
    @BindView(R.id.profileYaurl)
    EditText urlText;
    @BindView(R.id.profileYaReview)
    EditText reviewText;
    @BindView(R.id.profileYaZnatok)
    EditText znatokText;
    @BindView(R.id.profileYaBuyer)
    EditText buyerText;
    @BindView(R.id.profileYaPrice)
    EditText priceText;
    private String profileid = null;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_profile_yandex;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            url = getArguments().getString(EXTRA_URL, "");
            ambassadorViewModel.getSiteData().observe(getViewLifecycleOwner(), siteObserver);
            ambassadorViewModel.getSite(url, jwt);
        }
        ((MainActivity) getActivity()).setTitle("Яндекс");

        return view;
    }

    @Override
    protected void setResult(SiteDTO result) {
        urlText.setText(result.getUrl());
        reviewText.setText(result.getReviews());
        profileid = result.getProfileid();
        znatokText.setText(result.getYandexznatok());
        buyerText.setText(result.getYandexbuyer());
        priceText.setText(result.getPrice());
    }

    @OnClick(R.id.profileYaCheck)
    void onCheckButtonClicked() {
        if (isValidUrl(urlText.getText().toString(), "yandex")) {
            SiteDTO siteDTO = new SiteDTO(jwt);
            if ( profileid!=null) {
                siteDTO.setProfileid(profileid);
            }
            siteDTO.setUrl(urlText.getText().toString());
            siteDTO.setReviews(reviewText.getText().toString());
            siteDTO.setYandexznatok(znatokText.getText().toString());
            siteDTO.setYandexbuyer(buyerText.getText().toString());
            siteDTO.setPrice(priceText.getText().toString());

            ambassadorViewModel.updateSite(siteDTO);
        }
    }
}
