package com.kitapp.ambassador.fragments.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;

import com.kitapp.ambassador.R;
import com.kitapp.ambassador.dto.SiteDTO;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class BaseSiteFragment extends BaseSiteResponseHandlerFragment {
    static final String EXTRA_URL = "extra_url";

    protected Observer<SiteDTO> siteObserver;
    protected String url;

    @LayoutRes
    protected abstract int getLayoutResId();

    public static <T extends BaseSiteFragment> T newInstance(String url, Class<T> clazz) {
        T fragment = null;
        try {
            fragment = clazz.newInstance();
        } catch (IllegalAccessException | java.lang.InstantiationException e) {
            e.printStackTrace();
        }

        Bundle args = new Bundle();
        args.putString(EXTRA_URL, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(getLayoutResId(), container, false);
        setHasOptionsMenu(true);
        siteObserver = result -> {
            ambassadorViewModel.getSiteData().removeObserver(siteObserver);
            setResult(result);
        };

        return view;
    }

    protected abstract void setResult(SiteDTO result);

    protected boolean isValidUrl(String url, String siteName) {
        String regex = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
        Matcher matcher = Pattern.compile(regex, Pattern.CASE_INSENSITIVE).matcher(url);

        if (!matcher.find()) {
            Toast.makeText(getActivity(), "Невалидная ссылка", Toast.LENGTH_SHORT).show();
            return false;
        }
        try {
            boolean isValid = new URL(url).getHost().contains(siteName);
            if (!isValid) {
                Toast.makeText(getActivity(), "Невалидная ссылка", Toast.LENGTH_SHORT).show();
            }
            return isValid;
        } catch (MalformedURLException e) {
            Toast.makeText(getActivity(), "Невалидная ссылка", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.action_filter).setVisible(false);
        menu.findItem(R.id.action_search).setVisible(false);
    }
    // public boolean
}
