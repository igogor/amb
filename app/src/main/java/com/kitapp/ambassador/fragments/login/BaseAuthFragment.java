package com.kitapp.ambassador.fragments.login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.kitapp.ambassador.dto.net.BaseResult;
import com.kitapp.ambassador.viewmodels.AuthViewModel;

class BaseAuthFragment extends Fragment {
    AuthViewModel viewModel;
    Observer<BaseResult> messageObserver;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(getActivity()).get(AuthViewModel.class);
        messageObserver = result -> Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_LONG).show();

        if (!viewModel.getMessageData().hasActiveObservers()) {
            viewModel.getMessageData().observe(getViewLifecycleOwner(), messageObserver);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
