package com.kitapp.ambassador.fragments.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kitapp.ambassador.MainActivity;
import com.kitapp.ambassador.R;
import com.kitapp.ambassador.dto.SiteDTO;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TripadvisorFragment extends BaseSiteFragment {
    @BindView(R.id.profileTripurl)
    EditText urlText;
    @BindView(R.id.profiletripLevel)
    EditText levelText;
    @BindView(R.id.profileTripAwarads)
    EditText awardsText;
    @BindView(R.id.profileTripBal)
    EditText pointText;
    @BindView(R.id.profileTripThanks)
    EditText thanksbalText;
    @BindView(R.id.profilePrice)
    EditText priceText;
    private String profileid = null;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_profile_tripadvisor;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            url = getArguments().getString(EXTRA_URL, "");
            ambassadorViewModel.getSiteData().observe(getViewLifecycleOwner(), siteObserver);
            ambassadorViewModel.getSite(url, jwt);
        }
        ((MainActivity) getActivity()).setTitle("Tripadvisor");
        return view;
    }

    @Override
    protected void setResult(SiteDTO result) {
        urlText.setText(result.getUrl());
        levelText.setText(result.getLevel());
        profileid = result.getProfileid();
        awardsText.setText(result.getAwards());
        pointText.setText(result.getPoints());
        thanksbalText.setText(result.getGratitude());
        priceText.setText(result.getPrice());
    }

    @OnClick(R.id.profileTripCheck)
    void onCheckButtonClicked() {
        if (isValidUrl(urlText.getText().toString(), "tripadvisor")) {
            SiteDTO siteDTO = new SiteDTO(jwt);
            if ( profileid!=null) {
                siteDTO.setProfileid(profileid);
            }
            siteDTO.setUrl(urlText.getText().toString());
            siteDTO.setLevel(levelText.getText().toString());
            siteDTO.setAwards(awardsText.getText().toString());
            siteDTO.setPoints(pointText.getText().toString());
            siteDTO.setGratitude(thanksbalText.getText().toString());
            siteDTO.setPrice(priceText.getText().toString());
            ambassadorViewModel.updateSite(siteDTO);
        }
    }
}
