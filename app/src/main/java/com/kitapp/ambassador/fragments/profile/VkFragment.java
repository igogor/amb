package com.kitapp.ambassador.fragments.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kitapp.ambassador.MainActivity;
import com.kitapp.ambassador.R;
import com.kitapp.ambassador.dto.SiteDTO;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VkFragment extends BaseSiteFragment {
    @BindView(R.id.profileVkurl)
    EditText urlText;
    @BindView(R.id.profileVkFriend)
    EditText friendText;
    @BindView(R.id.profileVkSubscribers)
    EditText subscriberText;
    @BindView(R.id.profileVkPrice)
    EditText priceText;
    private String profileid = null;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_profile_vk;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            url = getArguments().getString(EXTRA_URL, "");
            ambassadorViewModel.getSiteData().observe(getViewLifecycleOwner(), siteObserver);
            ambassadorViewModel.getSite(url, jwt);
        }
        ((MainActivity) getActivity()).setTitle("Вконтакте");
        return view;
    }

    @Override
    protected void setResult(SiteDTO result) {
        urlText.setText(result.getUrl());
        profileid = result.getProfileid();
        friendText.setText(result.getFriends());
        subscriberText.setText(result.getSubscribers());
        priceText.setText(result.getPrice());
    }

    @OnClick(R.id.profileVkCheck)
    void onCheckButtonClicked() {
        if (isValidUrl(urlText.getText().toString(), "vk.com")) {
            SiteDTO siteDTO = new SiteDTO(jwt);
            if ( profileid!=null) {
                siteDTO.setProfileid(profileid);
            }
            siteDTO.setUrl(urlText.getText().toString());
            siteDTO.setFriends(friendText.getText().toString());
            siteDTO.setSubscribers(subscriberText.getText().toString());
            siteDTO.setPrice(priceText.getText().toString());

            ambassadorViewModel.updateSite(siteDTO);
        }
    }
}
