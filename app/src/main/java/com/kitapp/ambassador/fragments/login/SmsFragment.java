package com.kitapp.ambassador.fragments.login;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;

import com.kitapp.ambassador.BuildConfig;
import com.kitapp.ambassador.R;
import com.kitapp.ambassador.dto.AuthDTO;
import com.kitapp.ambassador.dto.SmsHandler;
import com.kitapp.ambassador.dto.net.SmsResult;
import com.kitapp.ambassador.dto.net.SmsStatusResult;
import com.kitapp.ambassador.helpers.Constants;
import com.kitapp.ambassador.receivers.SmsReceiver;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class SmsFragment extends BaseAuthFragment implements SmsHandler {
    private static final int REQUEST_READ_SMS = 100;
    @BindView(R.id.smsResponse)
    EditText responseText;
    @BindView(R.id.smsTimerText)
    TextView timerText;
    @BindView(R.id.smsRepeat)
    Button repeatButton;
    @BindView(R.id.hintText)
    TextView hintText;

    private AuthClicked listener;
    private AuthDTO auth;
    private CountDownTimer timer35Sec;
    private boolean is35SecGone = false;
    private Observer<SmsStatusResult> smsObserver;
    private Observer<SmsResult> sendSmsObserver;
    private SmsReceiver smsReceiver;
    private Intent receiverIntent;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AuthClicked) {
            listener = (AuthClicked) context;
        } else {
            throw new ClassCastException("$context must implement AuthClicked interface");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_login_sms, container, false);
        ButterKnife.bind(this, view);

        smsObserver = smsResult -> listener.onButtonClick(Constants.LOGIN_REGISTRATION_FRAGMENT);


        sendSmsObserver = smsResult -> {
            auth.setSmsCode(smsResult.getCode());
            timerText.setText("35");
            timer35Sec.start();
            repeatButton.setEnabled(false);
            is35SecGone = false;
            if (BuildConfig.DEBUG)
                Toast.makeText(getActivity(), "Код - "+smsResult.getCode(), Toast.LENGTH_SHORT).show();

            viewModel.getSendSms().removeObserver(sendSmsObserver);
        };

        viewModel.getAuthData().observe(getViewLifecycleOwner(), auth -> SmsFragment.this.auth = auth);
        viewModel.getSmsStatusData().observe(getViewLifecycleOwner(), smsObserver);
        viewModel.getMessageData().observe(getViewLifecycleOwner(), result ->
                Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_SHORT).show());

        hintText.setOnClickListener(v -> new SimpleTooltip.Builder(getActivity())
                .anchorView(v)
                .text("Tooltip text")
                .gravity(Gravity.END)
                .build().show());
        
        createTimer();
        checkPermission();
        return view;
    }

    private void createTimer() {
        timer35Sec = new CountDownTimer(35_000, 1_000) {
            @Override
            public void onTick(long l) {
                timerText.setText(String.valueOf(l / 1000));
            }

            @Override
            public void onFinish() {
                repeatButton.setEnabled(true);
                timerText.setText("");
                is35SecGone = true;
            }
        };
    }

    private void registerReceiver() {
        IntentFilter filter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        smsReceiver = new SmsReceiver(this);
        receiverIntent = getActivity().registerReceiver(smsReceiver, filter);
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.RECEIVE_SMS)) {
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.RECEIVE_SMS}, REQUEST_READ_SMS);
            }
        } else {
            registerReceiver();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_SMS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    registerReceiver();
                }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onResume() {
        super.onResume();
        responseText.setText("");
        timer35Sec.start();
        repeatButton.setEnabled(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        responseText.setText("");
        timer35Sec.cancel();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (receiverIntent != null)
            getActivity().unregisterReceiver(smsReceiver);
    }

    @OnClick(R.id.smsContinue)
    void onContinueClicked() {
        String inputValue = responseText.getText().toString().trim();
        if (!TextUtils.isEmpty(inputValue) && auth.getSmsCode().equals(inputValue) && !is35SecGone) {
            viewModel.smsStatus(auth.getPhoneNumber(), String.valueOf(auth.getSmsId()));
        } else {
            Toast.makeText(getActivity(), "Неправильный код", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.smsRepeat)
    void onRepeatClicked() {
        viewModel.getSendSms().observe(getViewLifecycleOwner(), sendSmsObserver);
        viewModel.sendSms(auth.getPhoneNumber());
    }

    @Override
    public void handleSms(String phoneSender, String text, Date timestamp) {
        responseText.setText(text);
    }
}
