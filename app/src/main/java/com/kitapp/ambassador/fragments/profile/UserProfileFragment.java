package com.kitapp.ambassador.fragments.profile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kitapp.ambassador.CustomerActivity;
import com.kitapp.ambassador.MainActivity;
import com.kitapp.ambassador.R;
import com.kitapp.ambassador.adapter.ProfileSocialAdapter;
import com.kitapp.ambassador.adapter.ProfileSocialCompleteAdapter;
import com.kitapp.ambassador.dto.SiteDTO;
import com.kitapp.ambassador.dto.SocialCompleteVO;
import com.kitapp.ambassador.dto.SocialVO;
import com.kitapp.ambassador.dto.net.UserProfileResult;
import com.kitapp.ambassador.helpers.Common;
import com.kitapp.ambassador.helpers.Constants;
import com.kitapp.ambassador.helpers.ParseSocialNetworks;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class UserProfileFragment extends BaseSiteResponseHandlerFragment {
    private static final int REQUEST_PHOTO = 2;
    private static final String EXTRA_PROFILE = UserProfileFragment.class.getName() + "client_profile";
    @BindView(R.id.profile_image)
    CircleImageView imageView;
    @BindView(R.id.profile_name)
    TextView nameView;
    @BindView(R.id.profileRatingBar)
    RatingBar ratingBarView;
    @BindView(R.id.profileRating)
    TextView ratingView;
    @BindView(R.id.socialRecycler)
    RecyclerView socialRecyclerView;
    @BindView(R.id.socialDoneRecycler)
    RecyclerView socialRecyclerDoneView;
    @BindView(R.id.editProfileData)
    TextView editProfile;
    @BindView(R.id.ambassadorHint)
    ImageView hintText;
    @BindView(R.id.textView4)
    TextView capabilityText;
    @BindView(R.id.imageView9)
    ImageView lineView;

    private List<SocialVO> networks;
    private List<SocialCompleteVO> networksDone = new ArrayList<>();
    private ProfileSocialCompleteAdapter doneAdapter;
    private ProfileSocialAdapter adapter;
    private UserProfileResult profileData;
    private ArrayList<SocialCompleteVO> parsedData;
    private boolean isAmbassadorProfile;
    private Observer<UserProfileResult> observer;

    public static UserProfileFragment newInstance(boolean ambassadorProfile) {

        Bundle args = new Bundle();
        args.putBoolean(EXTRA_PROFILE, ambassadorProfile);
        UserProfileFragment fragment = new UserProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_profile_preview, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        isAmbassadorProfile = getArguments().getBoolean(EXTRA_PROFILE);
        if (isAmbassadorProfile) {
            TypedArray names = getResources().obtainTypedArray(R.array.networks);
            TypedArray icons = getResources().obtainTypedArray(R.array.network_icons);

            networks = new ArrayList<>();
            for (int i = 0; i < names.length(); i++) {
                SocialVO item = new SocialVO(i, names.getString(i), icons.getDrawable(i));
                networks.add(item);
            }

            names.recycle();
            icons.recycle();

            socialRecyclerView.setHasFixedSize(true);
            socialRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
            socialRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            adapter = new ProfileSocialAdapter(networks, listener);
            socialRecyclerView.setAdapter(adapter);

            socialRecyclerDoneView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
            socialRecyclerDoneView.setLayoutManager(new LinearLayoutManager(getActivity()));
            doneAdapter = new ProfileSocialCompleteAdapter(getActivity(), networksDone, listener);
            socialRecyclerDoneView.setAdapter(doneAdapter);

            hintText.setOnClickListener(view1 -> new SimpleTooltip.Builder(getActivity())
                    .anchorView(view1)
                    .text("Tooltip text")
                    .gravity(Gravity.TOP)
                    .build().show()
            );
        } else {
            hintText.setVisibility(View.GONE);
            capabilityText.setVisibility(View.GONE);
            lineView.setVisibility(View.GONE);
        }
        getData();

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.action_filter).setVisible(false);
        menu.findItem(R.id.action_search).setVisible(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).setMainIcon();
            ((MainActivity) getActivity()).setTitle(getString(R.string.tab6_text));
        } else if (getActivity() instanceof CustomerActivity) {
            ((CustomerActivity) getActivity()).setMainIcon();
            ((CustomerActivity) getActivity()).setTitle(getString(R.string.tab6_text));
        }
    }

    private void getData() {
        observer = data -> {
            if (data.getRole() != null) {
                profileData = data;
                if (data.getPhoto() != null)
                    imageView.setImageBitmap(Common.convertToImage(data.getPhoto()));
                nameView.setText(data.getFullname());
                if (data.getRating() != null) {
                    ratingView.setText(data.getRating());
                    ratingBarView.setRating(Float.parseFloat(data.getRating()));
                }
                if (isAmbassadorProfile) {
                    parsedData = parseAccounts(data.getAccounts());
                    networksDone.clear();
                    networksDone.addAll(parsedData);
                    doneAdapter.notifyDataSetChanged();
                }
            }
        };
        ambassadorViewModel.getUserProfile(jwt).observe(getViewLifecycleOwner(), observer);
    }

    private ArrayList<SocialCompleteVO> parseAccounts(List<SiteDTO> accounts) {
        ArrayList<SocialCompleteVO> parsedData = new ArrayList<>(accounts.size());
        Resources res = getActivity().getResources();
        ArrayList<String> resultArray;
        for (SiteDTO item : accounts) {
            SocialVO socialVo = getNetworkByName(item.getSitename());
            SocialCompleteVO resultVO;
            switch (item.getSitename()) {
                case Constants.FACEBOOK:
                    resultArray = ParseSocialNetworks.parse(Constants.FACEBOOK, item, res);
                    resultVO = new SocialCompleteVO(socialVo, resultArray);
                    parsedData.add(resultVO);
                    break;
                case Constants.TRIPADVISOR:
                    resultArray = ParseSocialNetworks.parse(Constants.TRIPADVISOR, item, res);
                    resultVO = new SocialCompleteVO(socialVo, resultArray);
                    parsedData.add(resultVO);
                    break;
                case Constants.GOOGLE:
                    resultArray = ParseSocialNetworks.parse(Constants.GOOGLE, item, res);
                    resultVO = new SocialCompleteVO(socialVo, resultArray);
                    parsedData.add(resultVO);
                    break;
                case Constants.YANDEX:
                    resultArray = ParseSocialNetworks.parse(Constants.YANDEX, item, res);
                    resultVO = new SocialCompleteVO(socialVo, resultArray);
                    parsedData.add(resultVO);
                    break;
                case Constants.OTZOVIK:
                    resultArray = ParseSocialNetworks.parse(Constants.OTZOVIK, item, res);
                    resultVO = new SocialCompleteVO(socialVo, resultArray);
                    parsedData.add(resultVO);
                    break;
                case Constants.IRECOMMEND:
                    resultArray = ParseSocialNetworks.parse(Constants.IRECOMMEND, item, res);
                    resultVO = new SocialCompleteVO(socialVo, resultArray);
                    parsedData.add(resultVO);
                    break;
                case Constants.VK:
                    resultArray = ParseSocialNetworks.parse(Constants.VK, item, res);
                    resultVO = new SocialCompleteVO(socialVo, resultArray);
                    parsedData.add(resultVO);
                    break;
                case Constants.INSTAGRAM:
                    resultArray = ParseSocialNetworks.parse(Constants.INSTAGRAM, item, res);
                    resultVO = new SocialCompleteVO(socialVo, resultArray);
                    parsedData.add(resultVO);
                    break;
                case Constants.TWITTER:
                    resultArray = ParseSocialNetworks.parse(Constants.TWITTER, item, res);
                    resultVO = new SocialCompleteVO(socialVo, resultArray);
                    parsedData.add(resultVO);
                    break;
                default:
                    resultArray = ParseSocialNetworks.parse(Constants.OTHER, item, res);
                    resultVO = new SocialCompleteVO(socialVo, resultArray);
                    parsedData.add(resultVO);
                    break;
            }
            if (!item.getSitename().equals("Другой"))
                removeAddedNetwork(item.getSitename());
        }
        return parsedData;
    }

    private SocialVO getNetworkByName(String name) {
        for (SocialVO item : networks) {
            if (item.getName().equals(name)) {
                return item;
            }
        }
        return networks.get(networks.size() - 1);
    }

    private void removeAddedNetwork(String name) {
        for (int i = 0; i < networks.size() - 1; i++) {
            SocialVO network = networks.get(i);
            if (network.getName().equals(name)) {
                networks.remove(i);
                adapter.notifyDataSetChanged();
                break;
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.editProfileData)
    void OnEditProfileClicked() {
        listener.onSocNetAdd(10, profileData);
    }

    @OnClick(R.id.addPhotoButton)
    void onChangePhotoClicked() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);

        startActivityForResult(Intent.createChooser(intent, "Выберите фото"), REQUEST_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_PHOTO) {
                Uri selectedImage = data.getData();
                imageView.setImageURI(selectedImage);

                InputStream imageStream = null;
                try {
                    imageStream = getActivity().getContentResolver().openInputStream(selectedImage);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
                bitmap = Bitmap.createScaledBitmap(bitmap, 512, 512, false);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos);
                byte[] b = baos.toByteArray();
               /* long size = b.length;
                    if (size/1024 > 3500) {
                        Toast.makeText(getActivity(), "Image size more than 3.5 MB", Toast.LENGTH_SHORT).show();
                    } else {*/
                String encodedImage = Base64.encodeToString(b, Base64.NO_WRAP);
                ambassadorViewModel.updateUserPhoto(encodedImage, jwt);
                //}
            }
        }
    }

}
