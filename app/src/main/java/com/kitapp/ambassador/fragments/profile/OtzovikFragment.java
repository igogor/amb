package com.kitapp.ambassador.fragments.profile;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kitapp.ambassador.MainActivity;
import com.kitapp.ambassador.R;
import com.kitapp.ambassador.dto.SiteDTO;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OtzovikFragment extends BaseSiteFragment {
    @BindView(R.id.profileOtzurl)
    EditText urlText;
    @BindView(R.id.profileOtzPoints)
    EditText pointsText;
    @BindView(R.id.profileOtzComment)
    EditText postText;
    @BindView(R.id.profileOtzPrice)
    EditText priceText;
    private String profileid = null;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_profile_otzovik;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            url = getArguments().getString(EXTRA_URL, "");
            ambassadorViewModel.getSiteData().observe(getViewLifecycleOwner(), siteObserver);
            ambassadorViewModel.getSite(url, jwt);
        }
        ((MainActivity) getActivity()).setTitle("Otzovik");
        return view;
    }

    @Override
    protected void setResult(SiteDTO result) {
        urlText.setText(result.getUrl());
        pointsText.setText(result.getPoints());
        Log.i("wtf","prof id" + result.getProfileid());
        profileid = result.getProfileid();
        postText.setText(result.getPosts());
        priceText.setText(result.getPrice());
    }

    @OnClick(R.id.profileOtzCheck)
    void onCheckButtonClicked() {
        if (isValidUrl(urlText.getText().toString(), "otzovik")) {
            SiteDTO siteDTO = new SiteDTO(jwt);
            if ( profileid!=null) {
                siteDTO.setProfileid(profileid);
            }
            siteDTO.setUrl(urlText.getText().toString());
            siteDTO.setPoints(pointsText.getText().toString());
            siteDTO.setPosts(postText.getText().toString());
            siteDTO.setPrice(priceText.getText().toString());
            ambassadorViewModel.updateSite(siteDTO);
        }
    }
}
