package com.kitapp.ambassador.fragments.profile;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kitapp.ambassador.MainActivity;
import com.kitapp.ambassador.R;
import com.kitapp.ambassador.dto.SiteDTO;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OtherFragment extends BaseSiteFragment {
    @BindView(R.id.profileOtherurl)
    EditText urlText;
    @BindView(R.id.profileOtherTitle)
    EditText titleText;
    @BindView(R.id.profileOtherPrice)
    EditText priceText;
    @BindView(R.id.profileOtherDesc)
    EditText descText;
    private String profileid = null;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_profile_other;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            url = getArguments().getString(EXTRA_URL, "");
            ambassadorViewModel.getSiteData().observe(getViewLifecycleOwner(), siteObserver);
            ambassadorViewModel.getSite(url, jwt);
        }
        ((MainActivity) getActivity()).setTitle("Другой");
        return view;
    }

    @Override
    protected void setResult(SiteDTO result) {
        Log.i("wtf", "data " + result.toString());
        urlText.setText(result.getUrl());
        titleText.setText(result.getSitename());
        profileid = result.getProfileid();
        priceText.setText(result.getPrice());
        descText.setText(result.getDesc());
    }

    @OnClick(R.id.profileOtherCheck)
    void onCheckButtonClicked() {
        if (isValidUrl(urlText.getText().toString(), "")) {
            SiteDTO siteDTO = new SiteDTO(jwt);
            siteDTO.setUrl(urlText.getText().toString());
            siteDTO.setSitename("Другой");
            siteDTO.setPrice(priceText.getText().toString());
            siteDTO.setDesc(descText.getText().toString());
            if ( profileid!=null) {
                siteDTO.setProfileid(profileid);
            }
            ambassadorViewModel.updateSite(siteDTO);
        }
    }

    @OnClick(R.id.profileOtherAddSite)
    void onAddButtonClicked() {
        Toast.makeText(getActivity(), "Not Implement", Toast.LENGTH_SHORT).show();
    }
}
