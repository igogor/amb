package com.kitapp.ambassador.fragments.login;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.textfield.TextInputEditText;
import com.kitapp.ambassador.R;
import com.kitapp.ambassador.dto.AuthDTO;
import com.kitapp.ambassador.helpers.Constants;
import com.kitapp.ambassador.helpers.Validators;
import com.kitapp.ambassador.viewmodels.AuthViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class RegistrationFragment extends Fragment {
    @BindView(R.id.regPassword)
    TextInputEditText passwordText;
    @BindView(R.id.regPassword2)
    TextInputEditText repeatPasswordText;
    @BindView(R.id.hintText)
    TextView hintText;

    private AuthClicked listener;
    private AuthViewModel viewModel;
    private AuthDTO auth;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AuthClicked) {
            listener = (AuthClicked) context;
        } else {
            throw new ClassCastException("$context must implement AuthClicked interface");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_login_registration, container, false);
        ButterKnife.bind(this, view);
        viewModel = ViewModelProviders.of(getActivity()).get(AuthViewModel.class);
        viewModel.getAuthData().observe(getViewLifecycleOwner(), auth -> RegistrationFragment.this.auth = auth);
        hintText.setOnClickListener(v -> new SimpleTooltip.Builder(getActivity())
                .anchorView(v)
                .text("Tooltip text")
                .gravity(Gravity.END)
                .build().show());
        return view;
    }

    @OnClick(R.id.regSignin)
    void onRegisterSigninClicked() {
        if (passwordsEquals() && validateForm()) {
            auth.setPassword(getPassword());
            viewModel.getAuthData().postValue(auth);
            listener.onButtonClick(Constants.LOGIN_ROLE_FRAGMENT);
        }
    }

    private boolean validateForm() {
        return Validators.validatePassword(getPassword(), passwordText) &&
                Validators.validatePassword(getRepeatPassword(), repeatPasswordText);
    }

    private boolean passwordsEquals() {
        boolean valid = true;
        if (!getPassword().equals(getRepeatPassword())) {
            passwordText.setError(Constants.PASSWORD_DOESNT_MATCH);
            repeatPasswordText.setError(Constants.PASSWORD_DOESNT_MATCH);
            valid = false;
        } else {
            passwordText.setError(null);
            repeatPasswordText.setError(null);
        }
        return valid;
    }

    private String getPassword() {
        return passwordText.getText().toString().trim();
    }

    private String getRepeatPassword() {
        return repeatPasswordText.getText().toString().trim();
    }
}
