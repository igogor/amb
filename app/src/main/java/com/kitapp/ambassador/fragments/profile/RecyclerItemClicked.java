package com.kitapp.ambassador.fragments.profile;

import com.kitapp.ambassador.dto.net.UserProfileResult;

public interface RecyclerItemClicked {
    void onSocNetAdd(int index, UserProfileResult profileData);
    void onSocNetEdit(String nameSite, String url);
    void dataSaved();
}
