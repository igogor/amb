package com.kitapp.ambassador.fragments.login;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.kitapp.ambassador.R;
import com.kitapp.ambassador.dto.AuthDTO;
import com.kitapp.ambassador.helpers.Constants;
import com.kitapp.ambassador.viewmodels.AuthViewModel;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class RoleFragment extends Fragment {

    private AuthClicked listener;
    private AuthViewModel viewModel;
    private AuthDTO auth;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AuthClicked) {
            listener = (AuthClicked) context;
        } else {
            throw new ClassCastException("$context must implement AuthClicked interface");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_login_role, container, false);
        ButterKnife.bind(this, view);
        viewModel = ViewModelProviders.of(getActivity()).get(AuthViewModel.class);
        viewModel.getAuthData().observe(getViewLifecycleOwner(), auth -> RoleFragment.this.auth = auth);
        return view;
    }

    @OnClick(R.id.roleButtonCustomer)
    void onCustomerClicked() {
        auth.setRole(Constants.ROLE_CLIENT);
        viewModel.getAuthData().postValue(auth);
        goToNextScreen();

    }

    @OnClick(R.id.roleButtonExecutor)
    void onExecutorClicked() {
        auth.setRole(Constants.ROLE_AMBASSADOR);
        viewModel.getAuthData().postValue(auth);
        goToNextScreen();
    }

    private void goToNextScreen() {
        if (auth.isAuthViaFacebook()) {
            listener.onButtonClick(Constants.LOGIN_PERSONAL_DATA_FACEBOOK);
        } else {
            listener.onButtonClick(Constants.LOGIN_PERSONAL_DATA);
        }
    }


}
