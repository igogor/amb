package com.kitapp.ambassador.fragments;

import com.kitapp.ambassador.dto.net.TasksResultAmbassador;

public interface IPopup {
    void showDialog(TasksResultAmbassador tasksResultAmbassador);
}
