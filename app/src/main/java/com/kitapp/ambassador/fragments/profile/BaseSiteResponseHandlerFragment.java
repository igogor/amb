package com.kitapp.ambassador.fragments.profile;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.kitapp.ambassador.dto.net.JwtResult;
import com.kitapp.ambassador.helpers.Constants;
import com.kitapp.ambassador.helpers.SharedPrefHelper;
import com.kitapp.ambassador.viewmodels.AmbassadorViewModel;

class BaseSiteResponseHandlerFragment extends Fragment {
    RecyclerItemClicked listener;
    AmbassadorViewModel ambassadorViewModel;
    String jwt;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (RecyclerItemClicked) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Must implement RecyclerItemClicked interface");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        jwt = SharedPrefHelper.getString(getActivity(), Constants.JWT_PREFERENCES, "");
        ambassadorViewModel = ViewModelProviders.of(getActivity()).get(AmbassadorViewModel.class);
        ambassadorViewModel.getUpdateSiteData().observe(getViewLifecycleOwner(), new Observer<JwtResult>() {
            @Override
            public void onChanged(JwtResult result) {
                if (result.getMessage().equals(Constants.SITE_PROFILE_WAS_UPDATED) || result.getMessage().equals(Constants.USER_WAS_UPDATED)) {
                    ambassadorViewModel.getUpdateSiteData().removeObserver(this);
                    jwt = result.getJwt();
                    SharedPrefHelper.setString(BaseSiteResponseHandlerFragment.this.getActivity(), Constants.JWT_PREFERENCES, jwt);
                    ambassadorViewModel.getUserProfile(jwt);
                    listener.dataSaved();
                }
            }
        });
        if (!ambassadorViewModel.getMessageData().hasActiveObservers()) {
            ambassadorViewModel.getMessageData().observe(getViewLifecycleOwner(), result -> {
                Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_SHORT).show();
            });
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
