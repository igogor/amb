package com.kitapp.ambassador.fragments.task;

import android.content.Context;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kitapp.ambassador.R;
import com.kitapp.ambassador.adapter.AmbassadorTasksWorkingAdapter;
import com.kitapp.ambassador.dto.modeltask.AmbasadorModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListWorkingAmbasadorsFragment extends Fragment {
    private static ListWorkingAmbasadorsFragment INSTANCE;
    @BindView(R.id.amb_list_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.workNum)
    TextView workNum;
    private AmbassadorTasksWorkingAdapter adapter = new AmbassadorTasksWorkingAdapter();
    private MutableLiveData<Pair<Integer, Integer>> sizeData = new MutableLiveData<>();
    private IWorkListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IWorkListener) {
            listener = (IWorkListener) context;
        } else {
            throw new ClassCastException("$context must implement IInvitesListener interface");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.amb_list_fragment, container, false);
        ButterKnife.bind(this, view);
        bindUI();
        return view;
    }

    private void bindUI() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        adapter.setListener(listener);
        adapter.setSizeLiveData(sizeData);
        workNum.setText(getString(R.string.your_task_not_executing));
        sizeData.observe(getViewLifecycleOwner(), sizePair -> {
            if (sizePair.second != 0) {
                workNum.setText(getResources().getString(R.string.execute_ambasadors));
                workNum.setText(String.format("Задание выполняют %d из %d амбасадоров", sizePair.second, sizePair.first)); //todo delete hardcoded text
            } else {
                workNum.setText(getString(R.string.your_task_not_executing));
            }
        });
    }

    public void setListWorking(List<AmbasadorModel> models, int sizeAll) {
        adapter.setData(models, sizeAll);
        Pair<Integer, Integer> pairSize = new Pair<>(sizeAll, models.size());
        sizeData.postValue(pairSize);
    }

    public void setData(int idTask, String jwt) {
        adapter.setData(idTask, jwt);
    }

    public static ListWorkingAmbasadorsFragment getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ListWorkingAmbasadorsFragment();
        }
        return INSTANCE;
    }
}
