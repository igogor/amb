package com.kitapp.ambassador.fragments.login;

public interface AuthClicked {
    void onButtonClick(int index);
}
