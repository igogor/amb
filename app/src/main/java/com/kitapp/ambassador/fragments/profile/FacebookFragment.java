package com.kitapp.ambassador.fragments.profile;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kitapp.ambassador.MainActivity;
import com.kitapp.ambassador.R;
import com.kitapp.ambassador.dto.SiteDTO;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FacebookFragment extends BaseSiteFragment {

    @BindView(R.id.profileFacebookurl)
    EditText urlText;
    @BindView(R.id.profileFacebookFriemds)
    EditText friendsText;
    @BindView(R.id.profileFacebookPrice)
    EditText priceText;
    private String profileid = null;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_profile_facebook;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            url = getArguments().getString(EXTRA_URL, "");
            ambassadorViewModel.getSiteData().observe(getViewLifecycleOwner(), siteObserver);
            ambassadorViewModel.getSite(url, jwt);
        }
        ((MainActivity) getActivity()).setTitle("Facebook");

        return view;
    }

    @OnClick(R.id.profileFacebookCheck)
    void onCheckButtonClicked() {
        if (isValidUrl(urlText.getText().toString(), "facebook")) {
            SiteDTO siteDTO = new SiteDTO(jwt);
            if (profileid != null) {
                siteDTO.setProfileid(profileid);
            }else {
                siteDTO.setProfileid("");
            }
            siteDTO.setUrl(urlText.getText().toString());
            siteDTO.setFriends(friendsText.getText().toString());
            siteDTO.setPrice(priceText.getText().toString());
            ambassadorViewModel.updateSite(siteDTO);
        }
    }

    @Override
    protected void setResult(SiteDTO result) {
        Log.i("wtf", "data " + result.toString());
        urlText.setText(result.getUrl());
        profileid = result.getProfileid();
        friendsText.setText(result.getFriends());
        priceText.setText(result.getPrice());
    }
}
