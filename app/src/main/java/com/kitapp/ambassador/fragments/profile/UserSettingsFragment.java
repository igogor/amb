package com.kitapp.ambassador.fragments.profile;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.textfield.TextInputEditText;
import com.kitapp.ambassador.CustomerActivity;
import com.kitapp.ambassador.MainActivity;
import com.kitapp.ambassador.R;
import com.kitapp.ambassador.dialogs.GenderDialog;
import com.kitapp.ambassador.dto.net.UserProfileResult;
import com.kitapp.ambassador.helpers.Validators;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserSettingsFragment extends BaseSiteResponseHandlerFragment {
    private static final String PROFILE_DATA = "profile_data";
    private static final int CHANGE_GENDER = 0;
    private static final String GENDER = "Gender";

    @BindView(R.id.profileSettingsName)
    TextInputEditText nameView;
    @BindView(R.id.profileSettingsSurname)
    TextInputEditText surnameView;
    @BindView(R.id.profileSettingsPhone)
    TextInputEditText phoneView;
    @BindView(R.id.profileSettingsEmail)
    TextInputEditText emailView;
    @BindView(R.id.profileSettingsBirthday)
    TextInputEditText birthdayView;
    @BindView(R.id.profileSettingsCity)
    TextInputEditText cityView;
    @BindView(R.id.profileSettingsGender)
    TextInputEditText genderView;

    private UserProfileResult data;
    private DatePickerDialog datePickerDialog;
    private Calendar calendar = Calendar.getInstance();

    public static UserSettingsFragment newInstance(UserProfileResult data) {

        Bundle args = new Bundle();
        args.putParcelable(PROFILE_DATA, data);
        UserSettingsFragment fragment = new UserSettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            data = getArguments().getParcelable(PROFILE_DATA);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_profile_settings, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        nameView.setText(data.getFirstname());
        surnameView.setText(data.getLastname());
        emailView.setText(data.getEmail());
        phoneView.setText(data.getPhone());
        birthdayView.setText(data.getBirthday());
        cityView.setText(data.getNativecity());
        genderView.setText(data.getGender());

        birthdayView.setOnClickListener((v) -> createDatePicker());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof MainActivity)
            ((MainActivity) getActivity()).setTitle("Личные данные");
        else if (getActivity() instanceof CustomerActivity)
            ((CustomerActivity) getActivity()).setTitle("Личные данные");

    }

    private void createDatePicker() {
        DatePickerDialog.OnDateSetListener dateListener = (datePicker, year, month, day) -> {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, day);
            updateLabel();
        };

        String[] date = data.getBirthday().split("\\.");
        datePickerDialog = new DatePickerDialog(getActivity(), dateListener, Integer.parseInt(date[2]), Integer.parseInt(date[1]), Integer.parseInt(date[0]));
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void updateLabel() {
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        birthdayView.setText(df.format(calendar.getTime()));
    }

    @OnClick(R.id.profileSettingsSaveButton)
    void onSaveButtonClick() {
        data.setJwt(jwt);
        if (dataChanged()) {
            ambassadorViewModel.updateUserProfile(data);
        } else {
            Toast.makeText(getContext(), "Вы не внесли изменений", Toast.LENGTH_SHORT).show();
            Objects.requireNonNull(getActivity()).onBackPressed();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.action_filter).setVisible(false);
        menu.findItem(R.id.action_search).setVisible(false);
    }

    @OnClick(R.id.profileSettingsGender)
    void onGenderClicked() {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        GenderDialog dialog = GenderDialog.newInstance(genderView.getText().toString());
        dialog.setTargetFragment(this, CHANGE_GENDER);
        dialog.show(manager, GENDER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CHANGE_GENDER) {
                genderView.setText(data.getStringExtra(GenderDialog.EXTRA_GENDER));
            }
        }
    }

    private boolean dataChanged() {
        String name = nameView.getText().toString();
        String surname = surnameView.getText().toString();
        String email = emailView.getText().toString();
        String birthday = birthdayView.getText().toString();
        String city = cityView.getText().toString();
        String gender = genderView.getText().toString();
        boolean isChanged = false;

        if (!name.equals(data.getFirstname())) {
            isChanged = true;
            data.setFirstname(name);
        }
        if (!surname.equals(data.getLastname())) {
            isChanged = true;
            data.setLastname(surname);
        }
        if (!email.equals(data.getEmail())) {
            if (Validators.validateEmail(emailView.getText().toString(), emailView)) {
                data.setEmail(email);
                isChanged = true;
            }
        }
        if (!birthday.equals(data.getBirthday())) {
            isChanged = true;
            data.setBirthday(birthday);
        }
        if (!city.equals(data.getNativecity())) {
            isChanged = true;
            data.setNativecity(city);
        }
        if (!gender.equals(data.getGender())) {
            isChanged = true;
            data.setGender(gender);
        }
        return isChanged;
    }
}
