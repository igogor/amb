package com.kitapp.ambassador.fragments.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kitapp.ambassador.MainActivity;
import com.kitapp.ambassador.R;
import com.kitapp.ambassador.dto.SiteDTO;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GoogleFragment extends BaseSiteFragment {
    @BindView(R.id.profileGoogleurl)
    EditText urlText;
    @BindView(R.id.profileGoogleLevel)
    EditText levelText;
    @BindView(R.id.profileGooglePoints)
    EditText pointText;
    @BindView(R.id.profileGoogleReview)
    EditText reviewText;
    @BindView(R.id.profileGooglePrice)
    EditText priceText;
    private String profileid = null;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_profile_google;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            url = getArguments().getString(EXTRA_URL, "");
            ambassadorViewModel.getSiteData().observe(getViewLifecycleOwner(), siteObserver);
            ambassadorViewModel.getSite(url, jwt);
        }
        ((MainActivity) getActivity()).setTitle("Google");
        return view;
    }

    @Override
    protected void setResult(SiteDTO result) {
        profileid = result.getProfileid();
        urlText.setText(result.getUrl());
        levelText.setText(result.getLevel());
        pointText.setText(result.getPoints());
        reviewText.setText(result.getReviews());
        priceText.setText(result.getPrice());
    }

    @OnClick(R.id.profileGoogleCheck)
    void onCheckButtonClicked() {
        if (isValidUrl(urlText.getText().toString(), "google")) {
            SiteDTO siteDTO = new SiteDTO(jwt);
            siteDTO.setUrl(urlText.getText().toString());
            if ( profileid!=null) {
                siteDTO.setProfileid(profileid);
            }else {
                siteDTO.setProfileid("");
            }
            siteDTO.setLevel(levelText.getText().toString());
            siteDTO.setReviews(reviewText.getText().toString());
            siteDTO.setPoints(pointText.getText().toString());
            siteDTO.setPrice(priceText.getText().toString());

            ambassadorViewModel.updateSite(siteDTO);
        }
    }
}
