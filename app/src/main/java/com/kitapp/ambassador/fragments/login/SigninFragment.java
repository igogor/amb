package com.kitapp.ambassador.fragments.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;

import com.google.android.material.textfield.TextInputEditText;
import com.kitapp.ambassador.CustomerActivity;
import com.kitapp.ambassador.MainActivity;
import com.kitapp.ambassador.R;
import com.kitapp.ambassador.RecoveryActivity;
import com.kitapp.ambassador.dto.net.JwtResult;
import com.kitapp.ambassador.helpers.Constants;
import com.kitapp.ambassador.helpers.SharedPrefHelper;
import com.kitapp.ambassador.helpers.Validators;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class SigninFragment extends BaseAuthFragment {
    @BindView(R.id.signPphoneNumber)
    EditText phoneText;
    @BindView(R.id.signPassword)
    TextInputEditText passwordText;
    @BindView(R.id.signSpinner)
    Spinner codeCountry;
    @BindView(R.id.hintText)
    TextView hintText;


    private AuthClicked listener;
    private Observer<JwtResult> signinObserver;
    private String[] codes;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AuthClicked) {
            listener = (AuthClicked) context;
        } else {
            throw new ClassCastException("$context must implement AuthClicked interface");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_login_signin, container, false);
        ButterKnife.bind(this, view);
        setHints();
        signinObserver = loginResult -> {
            if (loginResult.getMessage().equals(Constants.SUCCESSFUL_LOGIN)) {
                SharedPrefHelper.setString(getActivity(), Constants.JWT_PREFERENCES, loginResult.getJwt());
                viewModel.validateToken(loginResult.getJwt());
            }
            viewModel.loginData().removeObserver(signinObserver);
        };

        viewModel.getUserData().observe(this, result -> {
            if (result.getRole().equals(Constants.ROLE_AMBASSADOR)) {
                startActivity(new Intent(getActivity(), MainActivity.class));
            } else {
                startActivity(new Intent(getActivity(), CustomerActivity.class));
            }
            getActivity().finish();
        });

        hintText.setOnClickListener(v -> new SimpleTooltip.Builder(getActivity())
                .anchorView(v)
                .text("Tooltip text")
                .gravity(Gravity.END)
                .build().show());

        codes = getResources().getStringArray(R.array.login_code_region_array);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_textview, codes);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        codeCountry.setAdapter(adapter);
        return view;
    }

    @OnClick(R.id.signContinue)
    void onSigninClicked() {
        if (validateForm()) {
            String phoneNoMask = getFullPhone().replaceAll("[^+0-9]", "");
            viewModel.loginData().observe(getViewLifecycleOwner(), signinObserver);
            viewModel.login(phoneNoMask, getPassword());
        }
    }

    private String getFullPhone() {
        return codeCountry.getSelectedItem().toString() + getPhone();
    }

    @OnClick(R.id.signForgetPassword)
    void onForgetPasswordClicked() {
        if (!getPhone().isEmpty()) {
            if (Validators.validatePhoneNumber(getFullPhone(), phoneText)) {
                Intent intent = new Intent(getActivity(), RecoveryActivity.class);
                intent.putExtra(RecoveryActivity.EXTRA_POSITION, codeCountry.getSelectedItemPosition());
                intent.putExtra(RecoveryActivity.EXTRA_PHONE, getPhone());
                startActivity(intent);
            }
        } else {
            startActivity(new Intent(getActivity(), RecoveryActivity.class));
        }
    }

    private boolean validateForm() {
        return Validators.validatePhoneNumber(getFullPhone(), phoneText) &&
                Validators.validatePassword(getPassword(), passwordText);
    }

    private String getPassword() {
        return passwordText.getText().toString().trim();
    }

    private String getPhone() {
        return phoneText.getText().toString().trim();
    }

    /**set hints programmatically because on meizu we have crush when set hint in XML **/
    private void setHints(){
        passwordText.setHint(getString(R.string.login_auth_password));
    }
}
