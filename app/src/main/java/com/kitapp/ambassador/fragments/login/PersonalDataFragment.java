package com.kitapp.ambassador.fragments.login;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.edittextpicker.aliazaz.EditTextPicker;
import com.google.android.material.textfield.TextInputLayout;
import com.kitapp.ambassador.CustomerActivity;
import com.kitapp.ambassador.MainActivity;
import com.kitapp.ambassador.R;
import com.kitapp.ambassador.dto.AuthDTO;
import com.kitapp.ambassador.dto.net.BaseResult;
import com.kitapp.ambassador.dto.net.JwtResult;
import com.kitapp.ambassador.dto.net.UserProfileResult;
import com.kitapp.ambassador.helpers.Constants;
import com.kitapp.ambassador.helpers.SharedPrefHelper;
import com.kitapp.ambassador.helpers.Validators;
import com.kitapp.ambassador.viewmodels.AuthViewModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PersonalDataFragment extends Fragment implements LocationListener {
    private static final int REQUEST_PHONE_STATE = 0;
    private static final int REQUEST_COORDINATE = 1;
    @BindView(R.id.dataName)
    EditText nameText;
    @BindView(R.id.dataSurname)
    EditText surnameText;
    @BindView(R.id.dataSpinner)
    Spinner codeCountry;
    @BindView(R.id.dataPhone)
    EditText phoneText;
    @BindView(R.id.dataPhoneMask)
    EditTextPicker phoneTextMask;
    @BindView(R.id.dataEmail)
    EditText emailText;
    @BindView(R.id.dataBirthday)
    EditText birthdayText;
    @BindView(R.id.dataCity)
    EditText cityText;
    @BindView(R.id.dataMaleRadioButton)
    RadioButton maleRadioButton;
    @BindView(R.id.dataFemaleRadioButton)
    RadioButton femaleRadioButton;
    @BindView(R.id.dataTermsCheckBox)
    CheckBox termsCheckBoxNoText;
    @BindView(R.id.dataTermsText)
    TextView termsText;
    @BindView(R.id.readOnlyLayout)
    LinearLayout readLayout;
    @BindView(R.id.editOnlyLayout)
    TextInputLayout editLayout;
    @BindView(R.id.dataConfirm)
    Button confirmButton;

    private AuthViewModel viewModel;
    private AuthClicked listener;
    private AuthDTO auth = new AuthDTO();
    private Calendar calendar = Calendar.getInstance();
    private DatePickerDialog datePickerDialog;
    private Observer<BaseResult> observer;
    private Observer<UserProfileResult> profileObserver;
    private Observer<JwtResult> signinObserver;
    private String[] codes;
    private String gender = "";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AuthClicked) {
            listener = (AuthClicked) context;
        } else {
            throw new ClassCastException("$context must implement AuthClicked interface");
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_login_presonal_data, container, false);
        ButterKnife.bind(this, view);

        femaleRadioButton.setOnCheckedChangeListener((compoundButton, b) -> applyStyle(compoundButton, b));
        femaleRadioButton.setOnCheckedChangeListener((compoundButton, b) -> applyStyle(compoundButton, b));
        maleRadioButton.setOnCheckedChangeListener((compoundButton, b) -> applyStyle(compoundButton, b));
        maleRadioButton.performClick();

        createDatePicker();

        termsCheckBoxNoText.setOnCheckedChangeListener((compoundButton, b) -> confirmButton.setEnabled(b));

        confirmButton.setEnabled(false);
        birthdayText.setOnClickListener((v) -> datePickerDialog.show());
        cityText.setOnTouchListener((view1, motionEvent) -> {
            //final int DRAWABLE_LEFT = 0; final int DRAWABLE_TOP = 1;final int DRAWABLE_BOTTOM = 3;
            final int DRAWABLE_RIGHT = 2;

            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                if (motionEvent.getRawX() >= (cityText.getRight() - cityText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    requestGpsPermission();
                    return true;
                }
            }
            return false;
        });

        viewModel = ViewModelProviders.of(getActivity()).get(AuthViewModel.class);
        viewModel.getAuthData().observe(getViewLifecycleOwner(), auth -> {
            PersonalDataFragment.this.auth = auth;
            if (auth.getPhoneNumber().isEmpty()) {
                getCountryCodes();
                editLayout.setVisibility(View.GONE);
                readLayout.setVisibility(View.VISIBLE);
                nameText.setText(auth.getName());
                surnameText.setText(auth.getSurname());
                emailText.setText(auth.getEmail());
                if (!auth.getGender().equals("")) {
                    if (auth.getGender().equals("1")) {
                        femaleRadioButton.performClick();
                    } else {
                        maleRadioButton.performClick();
                    }
                }
                if (!auth.getBirthday().isEmpty())
                    birthdayText.setText(auth.getBirthday());
            } else {
                editLayout.setVisibility(View.VISIBLE);
                readLayout.setVisibility(View.GONE);
                phoneText.setText(auth.getPhoneNumberMask());
            }
        });
        observer = result -> {
            viewModel.getMessageData().removeObserver(observer);
            if (result.getMessage().equals(Constants.USER_WAS_CREATED))
                viewModel.login(auth.getPhoneNumber(), auth.getPassword());
            else
                Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_SHORT).show();
        };

        signinObserver = loginResult -> {
            viewModel.loginData().removeObserver(signinObserver);
            if (loginResult.getMessage().equals(Constants.SUCCESSFUL_LOGIN)) {
                SharedPrefHelper.setString(getActivity(), Constants.JWT_PREFERENCES, loginResult.getJwt());
                viewModel.validateToken(loginResult.getJwt());
            }
        };

        profileObserver = profile -> {
            viewModel.getUserData().removeObserver(profileObserver);
            if (profile.getRole().equals(Constants.ROLE_AMBASSADOR)) {
                startActivity(new Intent(getActivity(), MainActivity.class));
            } else {
                startActivity(new Intent(getActivity(), CustomerActivity.class));
            }
            getActivity().finish();
        };

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        new Handler().postDelayed(this::getImei, 6000);
    }

    private void getCountryCodes() {
        codes = getResources().getStringArray(R.array.login_code_region_array);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_textview, codes);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        codeCountry.setAdapter(adapter);
    }

    @Override
    public void onPause() {
        LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        lm.removeUpdates(this);
        super.onPause();
    }

    private void applyStyle(CompoundButton compoundButton, boolean selected) {
        if (selected) {
            compoundButton.setTextColor(getResources().getColor(R.color.radioButtonCheckedTextColor));
            compoundButton.setTextSize(18);
        } else {
            compoundButton.setTextColor(getResources().getColor(R.color.radioButtonUncheckedTextColor));
            compoundButton.setTextSize(14);
        }
        setGender(compoundButton);
    }

    private void createDatePicker() {
        DatePickerDialog.OnDateSetListener dateListener = (datePicker, year, month, day) -> {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, day);
            updateLabel();
        };

        datePickerDialog = new DatePickerDialog(getActivity(), dateListener, 1980, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }

    private void updateLabel() {
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        birthdayText.setText(df.format(calendar.getTime()));
    }

    @OnClick(R.id.dataMaleRadioButton)
    void dataMaleRadioButton(View v) {
        setGender(v);
    }

    @OnClick(R.id.dataFemaleRadioButton)
    void dataFemaleRadioButton(View v) {
        setGender(v);
    }

    @OnClick(R.id.dataConfirm)
    void confirmClick() {
        if (validateForm()) {
            auth.setName(nameText.getText().toString());
            auth.setSurname(surnameText.getText().toString());
            auth.setEmail(emailText.getText().toString());
            auth.setBirthday(birthdayText.getText().toString());
            auth.setCity(cityText.getText().toString());
            auth.setGender(this.gender);
            if (auth.getPhoneNumber().isEmpty()) {
                String inputPhone = codeCountry.getSelectedItem().toString() + phoneTextMask.getText().toString();
                String phoneNoMask = inputPhone.replaceAll("[^+0-9]", "");
                auth.setPhoneNumber(phoneNoMask);
                auth.setPhoneNumberMask(inputPhone);
            }
            if (auth.getPassword().isEmpty()) {
                auth.setPassword(generateString(8));
            }

            viewModel.getMessageData().observe(getViewLifecycleOwner(), observer);
            viewModel.getUserData().observe(getViewLifecycleOwner(), profileObserver);
            viewModel.loginData().observe(getViewLifecycleOwner(), signinObserver);
            viewModel.createUser(auth);
        }
    }

    private boolean validateForm() {
        boolean phoneValid = true;
        if (auth.getPhoneNumber().isEmpty()) {
            if (phoneTextMask.getText() == null) {
                phoneValid = false;
            } else {
                String inputPhone = codeCountry.getSelectedItem().toString() + phoneTextMask.getText().toString();
                if (!Validators.validatePhoneNumber(inputPhone, phoneTextMask)) {
                    phoneValid = false;
                }
            }
        }
        boolean nameValid =  Validators.isNotEmpty(nameText.getText().toString(), nameText);
        boolean surnameValid = Validators.isNotEmpty(surnameText.getText().toString(), surnameText);
        boolean emailValid = Validators.validateEmail(emailText.getText().toString(), emailText);
        boolean birthdayValid = Validators.isNotEmpty(birthdayText.getText().toString(), birthdayText);
        boolean cityValid = Validators.isNotEmpty(cityText.getText().toString(), cityText);
        boolean termsValid = Validators.isChecked(getActivity(), termsCheckBoxNoText);
        return nameValid && surnameValid && phoneValid && emailValid && birthdayValid && cityValid && termsValid;
    }

    private String generateString(int count) {
        final String ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final Random RANDOM = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < count; ++i) {
            sb.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
        }
        return sb.toString();
    }

    private void setGender(View v) {
        String gender = ((AppCompatRadioButton) v).getText().toString();
        if (auth != null) {
            this.gender = gender;
            auth.setGender(gender);
        }
    }

    private void getImei() {
        if (getActivity() != null) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                requestReadPhoneStatePermission();
            } else {
                permissionGranted();
            }
        }
    }

    private void requestGpsPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)) {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_COORDINATE);
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_COORDINATE);
            }
        } else {
            getLocation();
        }
    }

    private void requestReadPhoneStatePermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_PHONE_STATE)) {

            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_PHONE_STATE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PHONE_STATE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("~~~~~~~~", "PHONE GRANTED");
                    permissionGranted();
                } else {
                    Log.d("~~~~~~~~", "PHONE NO GRANTED");
                    auth.setImei("no data");
                }
            case REQUEST_COORDINATE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("~~~~~~~~", "LOCATION GRANTED");
                    getLocation();
                } else {
                    auth.setLat("no data");
                    auth.setLng("no data");
                }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @SuppressLint("MissingPermission")
    private void permissionGranted() {
        String imei;
        TelephonyManager tm = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (tm.getPhoneCount() == 2)
                    imei = tm.getImei(0);
                else
                    imei = tm.getImei();
            } else {
                if (tm.getPhoneCount() == 2)
                    imei = tm.getDeviceId(0);
                else
                    imei = tm.getDeviceId();
            }
        } else {
            imei = tm.getDeviceId();
        }
        auth.setImei(imei);
    }

    @SuppressLint("MissingPermission")
    private void getLocation() {
        Criteria criteria = new Criteria();
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setSpeedRequired(false);
        LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        String bestProvider = lm.getBestProvider(criteria, false);
        lm.requestLocationUpdates(bestProvider, 5000, 5, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        auth.setLat(String.valueOf(location.getLatitude()));
        auth.setLng(String.valueOf(location.getLongitude()));
        //TODO call server side to get name
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}