package com.kitapp.ambassador.fragments.task;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.tabs.TabLayout;
import com.kitapp.ambassador.CustomerActivity;
import com.kitapp.ambassador.R;
import com.kitapp.ambassador.dialogs.HintMessageDialog;
import com.kitapp.ambassador.dialogs.TaskRedactDialog;
import com.kitapp.ambassador.dto.SocialVO;
import com.kitapp.ambassador.fragments.customer.CustomerAmbassadorsListFragment;
import com.kitapp.ambassador.helpers.Constants;
import com.kitapp.ambassador.helpers.SharedPrefHelper;
import com.kitapp.ambassador.viewmodels.CustomerViewModel;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomerTaskPreviewFragment extends Fragment {
    private static final String EXTRA_ID = CustomerTaskPreviewFragment.class.getName() + "taskId";

    @BindView(R.id.tvTaskType)
    TextView taskType;
    @BindView(R.id.tvTaskTitle)
    TextView taskTitle;
    @BindView(R.id.tvTaskDescription)
    TextView taskDescription;
    @BindView(R.id.tvTaskInfoTitle1)
    TextView taskInfoTitle1;
    @BindView(R.id.tvTaskInfo1)
    TextView taskInfo1;
    @BindView(R.id.tvTaskInfoTitle2)
    TextView taskInfoTitle2;
    @BindView(R.id.tvTaskInfo2)
    TextView taskInfo2;
    @BindView(R.id.tvTaskInfoTitle3)
    TextView taskInfoTitle3;
    @BindView(R.id.tvTaskInfo3)
    TextView taskInfo3;
    @BindView(R.id.tvTaskLocation)
    TextView taskLocation;
    @BindView(R.id.tvTaskDeadline)
    TextView taskDeadline;
    @BindView(R.id.termText)
    TextView termText;
    @BindView(R.id.tvTaskPrice)
    TextView taskPrice;
    @BindView(R.id.tvTaskBonusTitle)
    TextView taskBonusTitle;
    @BindView(R.id.tvTaskBonus)
    TextView taskBonus;
    @BindView(R.id.amb_tabs)
    TabLayout tabLayout;
    @BindView(R.id.container_tab)
    RelativeLayout containerTab;
    @BindView(R.id.amb_container)
    FrameLayout ambContainer;
    @BindView(R.id.tvTaskStatus)
    TextView taskSatus;
    @BindView(R.id.imIconSite)
    ImageView iconSite;
    @BindView(R.id.tvLinkSite)
    TextView tvLinkSite;
    private int size;
    private String type = "";
    private String code = "";


    private List<SocialVO> networks = new ArrayList<>();
    private String jwt;
    private CustomerViewModel customerViewModel;
    private String url;
    private String taskId = null;
    private int activeTab = 0;

    public static CustomerTaskPreviewFragment newInstance(String id) {
        Bundle args = new Bundle();
        args.putString(EXTRA_ID, id);
        CustomerTaskPreviewFragment fragment = new CustomerTaskPreviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_client_task_view, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        // fab.hide();

        TypedArray names = getResources().obtainTypedArray(R.array.networks);
        TypedArray icons = getResources().obtainTypedArray(R.array.network_icons);
        initTabs();
        setChildFragment(ListWorkingAmbasadorsFragment.getInstance());
        for (int i = 0; i < names.length(); i++) {
            SocialVO item = new SocialVO(i, names.getString(i), icons.getDrawable(i));
            networks.add(item);
        }

        names.recycle();
        icons.recycle();

        jwt = SharedPrefHelper.getString(getActivity(), Constants.JWT_PREFERENCES, "");
        taskId = getArguments().getString(EXTRA_ID);
        customerViewModel = ViewModelProviders.of(getActivity()).get(CustomerViewModel.class);
        observeResult();
        customerViewModel.getTaskData().observe(getViewLifecycleOwner(), result -> {
            type = TaskType.getTitleByCode(Integer.parseInt(result.getTasktype()));
            code = result.getTasktype();
            ((CustomerActivity) getActivity()).setTitle(type);
            setImage(result);
            if (result.getBids().isEmpty() && result.getInvites().isEmpty() && result.getExecs().isEmpty()) {
                setVisibilityEmptyLists();
            } else {
                setVisibilityExistList();
            }
            if (result.getUrl() != null) {
                tvLinkSite.setText(result.getUrl());
                setLink(tvLinkSite);
            }
            ListWorkingAmbasadorsFragment.getInstance().setData(result.getId(), jwt);
            ListInvitationsAmbasadorsFragment.getInstance().setData(result.getId(), jwt);
            ListRequestAmbasadorsFragment.getInstance().setData(result.getId(), jwt);
            size = Integer.parseInt(result.getCount());
            ListWorkingAmbasadorsFragment.getInstance().setListWorking(result.getExecs(), size);
            ListInvitationsAmbasadorsFragment.getInstance().setListInvations(result.getInvites());
            ListRequestAmbasadorsFragment.getInstance().setListRequest(result.getBids());
            tabLayout.getTabAt(activeTab).select();
            taskType.setText(type);
            taskTitle.setText(result.getTitle());
            taskDescription.setText(result.getDescription());
            taskInfoTitle1.setText(result.getNotename1());
            taskInfo1.setText(result.getNote1());
            if (result.getNote1().isEmpty()) {
                taskInfoTitle1.setVisibility(View.GONE);
                taskInfo1.setVisibility(View.GONE);
            }
            taskInfoTitle2.setText(result.getNotename2());
            taskInfo2.setText(result.getNote2());
            if (result.getNote2().isEmpty()) {
                taskInfoTitle2.setVisibility(View.GONE);
                taskInfo2.setVisibility(View.GONE);
            }
            taskInfoTitle3.setText(result.getNotename3());
            taskInfo3.setText(result.getNote3());
            if (result.getNote3().isEmpty()) {
                taskInfoTitle3.setVisibility(View.GONE);
                taskInfo3.setVisibility(View.GONE);
            }
            String location = result.getSettings().getGeocity() + ", " + result.getSettings().getGeocountry();
            taskLocation.setText(location);
            if (result.getDeadlinehours().isEmpty() && !result.getDeadlinedays().isEmpty())
                taskDeadline.setText(getResources().getString(R.string.deadline_text_short, result.getDeadlinedays()));
            if (!result.getDeadlinehours().isEmpty() && !result.getDeadlinedays().isEmpty())
                taskDeadline.setText(getResources().getString(R.string.deadline_text_long, result.getDeadlinedays(), result.getDeadlinehours()));
            if (result.getDeadlinehours().isEmpty() && result.getDeadlinedays().isEmpty()) {
                taskDeadline.setVisibility(View.GONE);
                taskDeadline.setVisibility(View.GONE);
            }
            String sign = result.getCurrency();
            switch (sign) {
                case "RUR":
                    taskPrice.setText(getResources().getString(R.string.sign_ruble, result.getPrice()));
                    break;
                case "USD":
                    taskPrice.setText(getResources().getString(R.string.sign_dollar, result.getPrice()));
                    break;
                case "UAH":
                    taskPrice.setText(getResources().getString(R.string.sign_hrivna, result.getPrice()));
                    break;
            }
            if (result.getAlterprice().isEmpty()) {
                taskBonusTitle.setVisibility(View.GONE);
                taskBonus.setVisibility(View.GONE);
            } else {
                taskBonus.setText(result.getAlterprice());
            }
            url = result.getUrl();
        });
        customerViewModel.getTaskExData().observe(getViewLifecycleOwner(),
                taskType -> ListWorkingAmbasadorsFragment.getInstance().setListWorking(taskType.getExecs(), size));
        customerViewModel.getTaskInvitesData().observe(getViewLifecycleOwner(),
                taskType -> ListInvitationsAmbasadorsFragment.getInstance().setListInvations(taskType.getInvites()));
        customerViewModel.getTaskBidsData().observe(getViewLifecycleOwner(),
                taskType -> ListRequestAmbasadorsFragment.getInstance().setListRequest(taskType.getBids()));

        url = null;
        if (!jwt.isEmpty()) {
            customerViewModel.getTaskById(taskId, jwt);
        }
        ((CustomerActivity) getActivity()).enableBackButton();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((CustomerActivity) getActivity())
                .getSendInvationsResult()
                .observe(getViewLifecycleOwner(), message -> {
                    activeTab = 1;
                    tabLayout.getTabAt(activeTab).select();
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                });
    }

    @OnClick(R.id.bTaskInvite)
    void OnInviteButtonClicked(View v) {
        if (url != null) {
            try {
                String[] a = new URL(url).getHost().split("\\.");
                StringBuilder builder = new StringBuilder();
                String spaceDot = "";
                for (int i = 1; i < a.length; i++) {
                    builder.append(spaceDot);
                    spaceDot = ".";
                    builder.append(a[i]);
                }
                CustomerAmbassadorsListFragment fragment = CustomerAmbassadorsListFragment
                        .newInstance(builder.toString(), "friends", true, taskId, null);
                ((CustomerActivity) getActivity()).changeFragment(fragment);
            } catch (Exception ignored) {
                Toast.makeText(getContext(), "Ссылка не валидная", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.bTaskFinish)
    void OnFinishButtonClicked(View v) {
        customerViewModel
                .updateTask(taskId, "0", jwt).observe(getViewLifecycleOwner(),
                message -> {
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    getActivity().onBackPressed();
                });
    }

    private void initTabs() {
        String[] tabsArray = getResources().getStringArray(R.array.tabs_array);
        for (String s : tabsArray) {
            tabLayout.addTab(tabLayout.newTab().setText(s));
        }
        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        setChildFragment(ListWorkingAmbasadorsFragment.getInstance());
                        if (taskId != null && jwt != null) {
                            customerViewModel.getAmbsExecsTaskById(taskId, jwt);
                        }
                        break;
                    case 1:
                        setChildFragment(ListInvitationsAmbasadorsFragment.getInstance());
                        if (taskId != null && jwt != null) {
                            customerViewModel.getAmbsInvitesTaskById(taskId, jwt);
                        }
                        break;
                    case 2:
                        setChildFragment(ListRequestAmbasadorsFragment.getInstance());
                        if (taskId != null && jwt != null) {
                            customerViewModel.getAmbsBidsTaskById(taskId, jwt);
                        }
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    private void setChildFragment(Fragment fragment) {
        getChildFragmentManager().beginTransaction()
                .replace(R.id.amb_container, fragment)
                .commit();
    }

    private void setVisibilityEmptyLists() {
        containerTab.setVisibility(View.GONE);
        ambContainer.setVisibility(View.GONE);
        taskSatus.setVisibility(View.VISIBLE);
    }

    private void setVisibilityExistList() {
        containerTab.setVisibility(View.VISIBLE);
        ambContainer.setVisibility(View.VISIBLE);
        taskSatus.setVisibility(View.GONE);
    }

    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.action_filter).setVisible(false);
        menu.findItem(R.id.action_search).setVisible(false);
        menu.findItem(R.id.action_dots).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_dots:
                TaskRedactDialog dialog = TaskRedactDialog.newInstance(code, type, taskId);
                dialog.show(getChildFragmentManager(), "TaskRedact Dialog");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setLink(TextView tv) {
        final CharSequence text = tv.getText();
        final SpannableString spannableString = new SpannableString(text);
        spannableString.setSpan(new UnderlineSpan(), 0, spannableString.length(), 0);
        tv.setText(spannableString);
    }

    private void setImage(com.kitapp.ambassador.dto.modeltask.TaskType result) {
        if (result.getUrl() != null) {
            if (result.getUrl().contains("facebook")) {
                iconSite.setImageResource(R.drawable.ic_social_facebook);
            } else if (result.getUrl().contains("google")) {
                iconSite.setImageResource(R.drawable.ic_social_google);
            } else if (result.getUrl().contains("tripadvisor")) {
                iconSite.setImageResource(R.drawable.ic_social_tripadvisor);
            } else if (result.getUrl().contains("otzovik")) {
                iconSite.setImageResource(R.drawable.ic_social_otzovik);
            } else if (result.getUrl().contains("twitter")) {
                iconSite.setImageResource(R.drawable.ic_social_twitter);
            }
        }
    }

    private void observeResult() {
        customerViewModel.getLiveEventDelete()
                .observe(this, message -> {
                            HintMessageDialog
                                    .getInstance(message)
                                    .show(getActivity().getSupportFragmentManager(), "hint");
                          //  getActivity().onBackPressed();
                        }
                );
    }
}
