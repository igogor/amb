package com.kitapp.ambassador;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.kitapp.ambassador.dto.net.UserProfileResult;
import com.kitapp.ambassador.fragments.ambassador.AmbassadorBidsFragment;
import com.kitapp.ambassador.fragments.ambassador.AmbassadorTasksFragment;
import com.kitapp.ambassador.fragments.ambassador.TaskPreviewFragment;
import com.kitapp.ambassador.fragments.profile.FacebookFragment;
import com.kitapp.ambassador.fragments.profile.GoogleFragment;
import com.kitapp.ambassador.fragments.profile.InstagramFragment;
import com.kitapp.ambassador.fragments.profile.IrecommendFragment;
import com.kitapp.ambassador.fragments.profile.OtherFragment;
import com.kitapp.ambassador.fragments.profile.OtzovikFragment;
import com.kitapp.ambassador.fragments.profile.RecyclerItemClicked;
import com.kitapp.ambassador.fragments.profile.TripadvisorFragment;
import com.kitapp.ambassador.fragments.profile.TwitterFragment;
import com.kitapp.ambassador.fragments.profile.UserProfileFragment;
import com.kitapp.ambassador.fragments.profile.UserSettingsFragment;
import com.kitapp.ambassador.fragments.profile.VkFragment;
import com.kitapp.ambassador.fragments.profile.YandexFragment;
import com.kitapp.ambassador.fragments.task.ITaskClicked;
import com.kitapp.ambassador.helpers.Common;
import com.kitapp.ambassador.helpers.Constants;
import com.kitapp.ambassador.helpers.SharedPrefHelper;
import com.kitapp.ambassador.viewmodels.AmbassadorViewModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements RecyclerItemClicked, ITaskClicked,
        NavigationView.OnNavigationItemSelectedListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.bottom_nav)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    private AmbassadorViewModel ambassadorViewModel;
    private UserProfileResult profile;
    private String jwt;
    private BottomNavigationView.OnNavigationItemSelectedListener bottomListener;
    private ActionBarDrawerToggle drawerToggle;
    private boolean isBackBtnEnabled = false;
    private HeaderViewHolder headerViewHolder;
    private int currentIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);
        View header = navigationView.getHeaderView(0);
        headerViewHolder = new HeaderViewHolder(header);
        headerViewHolder.humbView.setOnClickListener(view1 -> {
            /*if (navigationView.getCheckedItem().getItemId() != R.id.nav_sites) {
               // goToSites(R.id.nav_sites);
                //navigationView.setCheckedItem(R.id.nav_sites);
            }*/
            drawer.closeDrawer(GravityCompat.START);
        });

        jwt = SharedPrefHelper.getString(this, Constants.JWT_PREFERENCES, "");

        View.OnClickListener menuHeaderListener = view -> {
            changeFragment(UserProfileFragment.newInstance(true));
            drawer.closeDrawers();
        };

        ambassadorViewModel = ViewModelProviders.of(MainActivity.this).get(AmbassadorViewModel.class);
        ambassadorViewModel.getUpdateSiteData().observe(this, result -> {
            if (result.getMessage().equals(Constants.ROLE_WAS_CHANGED)) {
                startActivity(new Intent(MainActivity.this, CustomerActivity.class));
                finish();
            }
        });
        ambassadorViewModel.getUserProfileData().observe(MainActivity.this, profile -> {
            MainActivity.this.profile = profile;
            if (profile.getPhoto() != null)
                headerViewHolder.photoView.setImageBitmap(Common.convertToImage(profile.getPhoto()));
            headerViewHolder.emailView.setText(profile.getFullname());

            headerViewHolder.photoView.setOnClickListener(menuHeaderListener);
            headerViewHolder.emailView.setOnClickListener(menuHeaderListener);
        });
        setSupportActionBar(toolbar);

        drawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        bottomListener = menuItem -> {
            //setSelection(menuItem.getItemId());
            switch (menuItem.getItemId()) {
                case R.id.nav_tasks:
                    changeFragment(AmbassadorTasksFragment.newInstance(true, null));
                    return true;
                case R.id.nav_my_tasks:
                    changeFragment(AmbassadorTasksFragment.newInstance(false, null));
                    return true;
                case R.id.nav_request:
                    changeFragment(new AmbassadorBidsFragment());
                    return true;
            }
            return false;
        };
        bottomNavigationView.setOnNavigationItemSelectedListener(bottomListener);

        changeFragment(UserProfileFragment.newInstance(true));
    }

    public void setSelection(int id) {
        navigationView.getMenu().findItem(id).setChecked(true);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        //setSelection(id);
        switch (id) {
            case R.id.nav_tasks:
                changeFragment(AmbassadorTasksFragment.newInstance(true, null));
                drawer.closeDrawers();
                return true;
            case R.id.nav_my_tasks:
                changeFragment(AmbassadorTasksFragment.newInstance(false, null));
                drawer.closeDrawers();
                return true;
            case R.id.nav_request:
                changeFragment(new AmbassadorBidsFragment());
                drawer.closeDrawers();
                return true;
            case R.id.nav_profile:
                changeFragment(UserProfileFragment.newInstance(true));
                drawer.closeDrawer(Gravity.LEFT);
                return true;
            case R.id.nav_exit:
                //TODO check if login via Facebook or another social network
                SharedPrefHelper.setString(MainActivity.this, Constants.JWT_PREFERENCES, "");
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
                return true;
            case R.id.nav_changerole:
                String newRole = profile.getRole().equals(Constants.ROLE_CLIENT) ? Constants.ROLE_AMBASSADOR : Constants.ROLE_CLIENT;
                ambassadorViewModel.changeUserRole(newRole, jwt);
                drawer.closeDrawer(Gravity.LEFT);
                return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.appbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_profile:
                item.setIcon(R.drawable.ic_toolbar_menu);
                return true;

            case android.R.id.home:
                if (isBackBtnEnabled) {
                    onBackPressed();
                    return true;
                } else {
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        drawer.openDrawer(GravityCompat.START);
                    }
                }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 1)
                finish();
            else
                getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onSocNetAdd(int index, UserProfileResult profile) {
        Fragment fragment;
        switch (index) {
            case 0: fragment = new FacebookFragment();
                break;
            case 1: fragment = new TripadvisorFragment();
                break;
            case 2: fragment = new GoogleFragment();
                break;
            case 3: fragment = new YandexFragment();
                break;
            case 4: fragment = new OtzovikFragment();
                break;
            case 5: fragment = new IrecommendFragment();
                break;
            case 6: fragment = new VkFragment();
                break;
            case 7: fragment = new InstagramFragment();
                break;
            case 8: fragment = new TwitterFragment();
                break;
            case 9: fragment = new OtherFragment();
                break;
            case 10: fragment = UserSettingsFragment.newInstance(profile);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + index);
        }
        changeFragment(fragment);
        enableBackButton();
    }

    @Override
    public void onSocNetEdit(String nameSite, String url) {
        Fragment fragment;

        switch (nameSite) {
            case "Facebook": fragment = FacebookFragment.newInstance(url, FacebookFragment.class);
                break;
            case "Tripadvisor": fragment = TripadvisorFragment.newInstance(url, TripadvisorFragment.class);
                break;
            case "Google": fragment = GoogleFragment.newInstance(url, GoogleFragment.class);
                break;
            case "Яндекс": fragment = YandexFragment.newInstance(url, YandexFragment.class);
                break;
            case "Otzovik": fragment = OtzovikFragment.newInstance(url, OtzovikFragment.class);
                break;
            case "Irecommend": fragment = IrecommendFragment.newInstance(url, IrecommendFragment.class);
                break;
            case "Вконтакте": fragment = VkFragment.newInstance(url, VkFragment.class);
                break;
            case "Instagram": fragment = InstagramFragment.newInstance(url, InstagramFragment.class);
                break;
            case "Twitter": fragment = TwitterFragment.newInstance(url, TwitterFragment.class);
                break;
            case "Другой": fragment = OtherFragment.newInstance(url, OtherFragment.class);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + nameSite);
        }
        changeFragment(fragment);
        enableBackButton();
    }

    public void enableBackButton() {
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        isBackBtnEnabled = true;
    }

    @Override
    public void dataSaved() {
        getSupportFragmentManager().popBackStack();
        setTitle(getString(R.string.tab6_text));
        setMainIcon();
    }

    public void setMainIcon() {
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_toolbar_menu);
        isBackBtnEnabled = false;
    }

    public void setTitle(String name) {
        getSupportActionBar().setTitle(name);
    }

    public void changeFragment(Fragment fragment) {
        String tag = fragment.getClass().getName();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.nav_host_fragment, fragment, tag)
                .addToBackStack(tag)
                .commit();
    }

    public String getActiveFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0)
            return "";

        return getSupportFragmentManager().getBackStackEntryAt(
                getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
    }

    @Override
    public void addTaskClicked() {

    }

    @Override
    public void offerTaskClicked(ArrayList<Integer> usersIds) {

    }

    @Override
    public void onTaskClicked(String id, String subtaskId) {
        TaskPreviewFragment fragment = TaskPreviewFragment.newInstance(id, subtaskId);
        changeFragment(fragment);
    }
}
