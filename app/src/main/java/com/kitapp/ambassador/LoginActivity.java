package com.kitapp.ambassador;

import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.facebook.FacebookSdk;
import com.kitapp.ambassador.fragments.login.AuthClicked;
import com.kitapp.ambassador.fragments.login.AuthFragment;
import com.kitapp.ambassador.fragments.login.PersonalDataFacebookFragment;
import com.kitapp.ambassador.fragments.login.PersonalDataFragment;
import com.kitapp.ambassador.fragments.login.RegistrationFragment;
import com.kitapp.ambassador.fragments.login.RoleFragment;
import com.kitapp.ambassador.fragments.login.SigninFragment;
import com.kitapp.ambassador.fragments.login.SmsFragment;
import com.kitapp.ambassador.viewmodels.AuthViewModel;
import com.vk.api.sdk.VK;
import com.vk.api.sdk.auth.VKAccessToken;
import com.vk.api.sdk.auth.VKAuthCallback;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.kitapp.ambassador.helpers.Constants.LOGIN_PERSONAL_DATA;
import static com.kitapp.ambassador.helpers.Constants.LOGIN_PERSONAL_DATA_FACEBOOK;
import static com.kitapp.ambassador.helpers.Constants.LOGIN_REGISTRATION_FRAGMENT;
import static com.kitapp.ambassador.helpers.Constants.LOGIN_ROLE_FRAGMENT;
import static com.kitapp.ambassador.helpers.Constants.LOGIN_SIGNIN_FRAGMENT;
import static com.kitapp.ambassador.helpers.Constants.LOGIN_SMS_FRAGMENT;

public class LoginActivity extends BaseActivity implements AuthClicked {
    @BindView(R.id.loginViewPager)
    ViewPager viewPager;
    @BindView(R.id.loginToolbar)
    Toolbar toolbar;
    @BindView(R.id.loginToolbarText)
    TextView toolbarText;

    private int numScreens = 7;
    private int currentIndex = 0;
    private LinkedList<Integer> returnIndex = new LinkedList<>();
    private TypedArray titles;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //TODO delete in release
        /*try {
            PackageInfo info = getPackageManager().getPackageInfo("com.kitapp.ambassador", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }*/
        FacebookSdk.sdkInitialize(getApplicationContext());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbarText.setText(R.string.login_auth_title);
        getSupportActionBar().setTitle(R.string.login_auth_title);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);

        ViewModelProviders.of(this).get(AuthViewModel.class).getErrorData()
                .observe(this, error -> Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show());

        Resources res = getResources();
        titles = res.obtainTypedArray(R.array.login_title_array);

        FragmentManager fragmentManager = getSupportFragmentManager();
        viewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case LOGIN_SIGNIN_FRAGMENT: return new SigninFragment();
                    case LOGIN_SMS_FRAGMENT: return new SmsFragment();
                    case LOGIN_REGISTRATION_FRAGMENT: return new RegistrationFragment();
                    case LOGIN_ROLE_FRAGMENT: return new RoleFragment();
                    case LOGIN_PERSONAL_DATA: return new PersonalDataFragment();
                    case LOGIN_PERSONAL_DATA_FACEBOOK: return new PersonalDataFacebookFragment();
                    default:
                        return new AuthFragment();
                }
            }

            @Override
            public int getCount() {
                return numScreens;
            }
        });
        viewPager.setOnTouchListener((view, motionEvent) -> true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        titles.recycle();
    }

    @Override
    public void onButtonClick(int index) {
        if (index < numScreens) {
            returnIndex.add(currentIndex);
            currentIndex = index;
            viewPager.setCurrentItem(index);
            if (index == LOGIN_ROLE_FRAGMENT) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            } else {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
            if (index == 6)
                setTitleText(titles.length()-1);
            else
                setTitleText(index);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            currentIndex = returnIndex.pollLast();
            viewPager.setCurrentItem(currentIndex);
            setTitleText(currentIndex);
            if (currentIndex == 0 || currentIndex == LOGIN_ROLE_FRAGMENT)
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AuthFragment.VK_SIGN_IN_CODE && resultCode == RESULT_OK) {
            VKAuthCallback callback = new VKAuthCallback() {
                @Override
                public void onLogin(@NotNull VKAccessToken vkAccessToken) {
                    data.putExtra(AuthFragment.VK_USER_ID, vkAccessToken.getUserId());

                    Fragment fragment = getSupportFragmentManager().getFragments().get(0);
                    if (fragment != null) {
                        fragment.onActivityResult(requestCode, resultCode, data);
                    }
                }

                @Override
                public void onLoginFailed(int i) {

                }
            };
            if (data == null || !VK.onActivityResult(requestCode, resultCode, data, callback)) {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private void setTitleText(int index) {
        toolbarText.setText(titles.getText(index));
    }
}
