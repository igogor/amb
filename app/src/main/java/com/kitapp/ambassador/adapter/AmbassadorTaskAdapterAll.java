package com.kitapp.ambassador.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kitapp.ambassador.R;
import com.kitapp.ambassador.dto.net.TasksResultAmbassador;
import com.kitapp.ambassador.fragments.IPopup;
import com.kitapp.ambassador.fragments.task.ITaskClicked;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AmbassadorTaskAdapterAll extends RecyclerView.Adapter<AmbassadorTaskAdapterAll.AmbassadorTaskViewHolder> {
    private Context context;
    private List<TasksResultAmbassador> list;
    private IPopup popupListener;
    private ITaskClicked taskListener;

    public AmbassadorTaskAdapterAll(Context context, List<TasksResultAmbassador> list, IPopup popupListener, ITaskClicked taskListener) {
        this.context = context;
        this.list = list;
        this.popupListener = popupListener;
        this.taskListener = taskListener;
    }

    @NonNull
    @Override
    public AmbassadorTaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_ambassador_task_list_item, parent, false);
        return new AmbassadorTaskViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AmbassadorTaskViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class AmbassadorTaskViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ambItemEstimateTime)
        TextView timeView;
        @BindView(R.id.ambItemType)
        TextView tipeView;
        @BindView(R.id.ambItemTitle)
        TextView titleView;
        @BindView(R.id.ambItemText)
        TextView textView;
        @BindView(R.id.ambItemPrice)
        TextView priceView;
        @BindView(R.id.ambItemLogo)
        ImageView imageView;
        @BindView(R.id.offerCandidate)
        TextView offerCandidateView;
        @BindView(R.id.tvBonus)
        TextView bonusView;

        private TasksResultAmbassador task;

        AmbassadorTaskViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            offerCandidateView.setOnClickListener(v -> popupListener.showDialog(task));
        }

        void bind(TasksResultAmbassador data) {
            this.task = data;
            if (data.getBidstatus() == null) {
                offerCandidateView.setText("Предложить \n кандидатуру");
            } else {
                if (data.getBidstatus().equals("1")) {
                    offerCandidateView.setText("Снять \n кандидатуру");
                } else {
                    offerCandidateView.setText("Предложить \n кандидатуру");
                }
            }
            String deadline = context.getString(R.string.deadline_text, data.getDeadline());
            if (data.getDeadline() != null) {
                timeView.setText(deadline);
            }
            tipeView.setText(context.getString(R.string.task_type, data.getTaskType()));
            titleView.setText(data.getTitle());
            textView.setText(data.getDescription());
            imageView.setImageDrawable(data.getIcon());
            if (!data.getAlterPrice().isEmpty())
                bonusView.setVisibility(View.VISIBLE);
            else
                bonusView.setVisibility(View.GONE);
            String sign = data.getCurrency();
            switch (sign) {
                case "RUR":
                    priceView.setText(context.getResources().getString(R.string.sign_ruble, data.getPrice()));
                    break;
                case "USD":
                    priceView.setText(context.getResources().getString(R.string.sign_dollar, data.getPrice()));
                    break;
                case "UAH":
                    priceView.setText(context.getResources().getString(R.string.sign_hrivna, data.getPrice()));
                    break;
            }
        }

        @OnClick(R.id.ambTaskPreview)
        void onPreviewTaskClicked(View v) {
            taskListener.onTaskClicked(String.valueOf(task.getId()), task.getSubtask());
        }
    }


}
