package com.kitapp.ambassador.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kitapp.ambassador.R;
import com.kitapp.ambassador.dto.SocialVO;
import com.kitapp.ambassador.fragments.profile.RecyclerItemClicked;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileSocialAdapter extends RecyclerView.Adapter<ProfileSocialAdapter.SocialViewHolder> {
    private List<SocialVO> list;
    private RecyclerItemClicked listener;

    public ProfileSocialAdapter(List<SocialVO> list, RecyclerItemClicked listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SocialViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_profile_list_item, parent, false);

        return new SocialViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SocialViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class SocialViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.snIconProfile)
        ImageView icon;
        @BindView(R.id.snTextProfile)
        TextView text;
        @BindView(R.id.snPlusButton)
        ImageView addButton;

        private SocialVO data;

        SocialViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            addButton.setOnClickListener(view -> listener.onSocNetAdd(data.getPosition(), null));
        }

        void bind(SocialVO data) {
            this.data = data;
            icon.setImageDrawable(data.getDrawable());
            text.setText(data.getName());
        }
    }


}
