package com.kitapp.ambassador.dto;

public class BaseDTO {
    private String jwt;

    public BaseDTO(String jwt) {
        this.jwt = jwt;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}
