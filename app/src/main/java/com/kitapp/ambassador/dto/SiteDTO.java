package com.kitapp.ambassador.dto;

public class SiteDTO extends BaseDTO{
    private String userId = "";  // - id
    private String url = "";  // - ссылка на профиль
    private String friends = "";  // - кол-во друзей
    private String price = "";  // - мин.цена задания
    private String currency = "";  // - валюта
    private String level = "";  // - уровень аккаунта
    private String points = "";  // - количество баллов
    private String awards = "";  // - награды
    private String gratitude = "";  // - благодарности
    private String reputation = "";  // - репутация
    private String reviews = "";  // - количество отзывов
    private String subscribers = "";  // - подписчики
    private String yandexznatok = "";  // - знаток города(уровень)
    private String yandexbuyer = "";  // - покупатель (уровень)
    private String lifetime = "";  // - срок на сайте
    private String posts = "";  // - количество постов
    private String sitename = "";  // - название сайта
    private String status = "";
    private String desc = "";
    private String profileid = "";

    public SiteDTO(String jwt) {
        super(jwt);
    }

    public void setProfileid(String profileid) {
        this.profileid = profileid;
    }

    public String getProfileid() {
        return profileid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFriends() {
        return friends;
    }

    public void setFriends(String friends) {
        this.friends = friends;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getAwards() {
        return awards;
    }

    public void setAwards(String awards) {
        this.awards = awards;
    }

    public String getGratitude() {
        return gratitude;
    }

    public void setGratitude(String gratitude) {
        this.gratitude = gratitude;
    }

    public String getReputation() {
        return reputation;
    }

    public void setReputation(String reputation) {
        this.reputation = reputation;
    }

    public String getReviews() {
        return reviews;
    }

    public void setReviews(String reviews) {
        this.reviews = reviews;
    }

    public String getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(String subscribers) {
        this.subscribers = subscribers;
    }

    public String getYandexznatok() {
        return yandexznatok;
    }

    public void setYandexznatok(String yandexznatok) {
        this.yandexznatok = yandexznatok;
    }

    public String getYandexbuyer() {
        return yandexbuyer;
    }

    public void setYandexbuyer(String yandexbuyer) {
        this.yandexbuyer = yandexbuyer;
    }

    public String getLifetime() {
        return lifetime;
    }

    public void setLifetime(String lifetime) {
        this.lifetime = lifetime;
    }

    public String getPosts() {
        return posts;
    }

    public void setPosts(String posts) {
        this.posts = posts;
    }

    public String getSitename() {
        return sitename;
    }

    public void setSitename(String sitename) {
        this.sitename = sitename;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
               return "userId=" + userId +
                ", url=" + url +
                ", profileid=" + profileid +
                ", sitename=" + sitename ;
    }
}
