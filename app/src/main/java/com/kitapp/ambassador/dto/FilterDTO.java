package com.kitapp.ambassador.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FilterDTO implements Parcelable {
    @SerializedName("sortbyname")
    @Expose
    private int sortByName;
    @SerializedName("sortbyrate")
    @Expose
    private int sortByRate;
    @SerializedName("favorite")
    @Expose
    private int favorite;
    @SerializedName("onlynew")
    @Expose
    private int onlyNew;
    @SerializedName("sortbyage")
    @Expose
    private int sortByAge;
    @SerializedName("gender")
    @Expose
    private int gender;
//    @SerializedName("online")
//    @Expose
//    private int online;
    @SerializedName("city")
    @Expose
    private String city;
    private String tasktypes;
    private String status;
    private String sortbyprice;
    private String clientcontent;
    private String geo;
    private String proof;
    private String sites;

    public FilterDTO() {
    }

    protected FilterDTO(Parcel in) {
        this.sortByName = in.readInt();
        this.sortByRate = in.readInt();
        this.favorite = in.readInt();
        this.onlyNew = in.readInt();
        this.sortByAge = in.readInt();
        this.gender = in.readInt();
      //  this.online = in.readInt();
        this.city = in.readString();
        this.tasktypes = in.readString();
        this.status = in.readString();
        this.sortbyprice = in.readString();
        this.clientcontent = in.readString();
        this.geo = in.readString();
        this.proof = in.readString();
        this.sites = in.readString();
    }

    public void setClientcontent(String clientcontent) {
        this.clientcontent = clientcontent;
    }

    public String getClientcontent() {
        return clientcontent;
    }

    public void setGeo(String geo) {
        this.geo = geo;
    }

    public void setSites(String sites) {
        this.sites = sites;
    }

    public String getSites() {
        return sites;
    }

    public String getGeo() {
        return geo;
    }

    public void setSortbyprice(String sortbyprice) {
        this.sortbyprice = sortbyprice;
    }

    public String getSortbyprice() {
        return sortbyprice;
    }

    public void setProof(String proof) {
        this.proof = proof;
    }

    public String getProof() {
        return proof;
    }

    public int getSortByName() {
        return sortByName;
    }

    public void setSortByName(int sortByName) {
        this.sortByName = sortByName;
    }

//    public void setOnline(int online) {
//        this.online = online;
//    }
//
//    public int getOnline() {
//        return online;
//    }

    public int getSortByRate() {
        return sortByRate;
    }

    public void setSortByRate(int sortByRate) {
        this.sortByRate = sortByRate;
    }

    public int getFavorite() {
        return favorite;
    }

    public void setFavorite(int favorite) {
        this.favorite = favorite;
    }

    public int getOnlyNew() {
        return onlyNew;
    }

    public void setOnlyNew(int onlyNew) {
        this.onlyNew = onlyNew;
    }

    public int getSortByAge() {
        return sortByAge;
    }

    public void setSortByAge(int sortByAge) {
        this.sortByAge = sortByAge;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setTasktypes(String tasktypes) {
        this.tasktypes = tasktypes;
    }

    public String getTasktypes() {
        return tasktypes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FilterDTO> CREATOR = new Creator<FilterDTO>() {
        @Override
        public FilterDTO createFromParcel(Parcel source) {
            return new FilterDTO(source);
        }

        @Override
        public FilterDTO[] newArray(int size) {
            return new FilterDTO[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.sortByName);
        dest.writeInt(this.sortByRate);
        dest.writeInt(this.favorite);
        dest.writeInt(this.onlyNew);
        dest.writeInt(this.sortByAge);
        dest.writeInt(this.gender);
     //   dest.writeInt(this.online);
        dest.writeString(this.city);
        dest.writeString(this.tasktypes);
        dest.writeString(this.status);
        dest.writeString(this.sortbyprice);
        dest.writeString(this.clientcontent);
        dest.writeString(this.geo);
        dest.writeString(this.proof);
        dest.writeString(this.sites);
    }
}