package com.kitapp.ambassador.dialogs;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.kitapp.ambassador.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TaskDialog extends DialogFragment {
    private static final String EXTRA_TITLE = TaskDialog.class.getName() + "popup_title";
    private static final String EXTRA_TEXT = TaskDialog.class.getName() + "popup_text";
    private static final String EXTRA_TASK_ID = TaskDialog.class.getName() + "task_id";
    private static final String EXTRA_STATUS = TaskDialog.class.getName() + "status";
    private ITaskListener listener;
    @BindView(R.id.popupTitle)
    TextView titleView;
    @BindView(R.id.popupText)
    TextView textView;

    public static TaskDialog newInstance(String title, String text, String taskId, String status) {
        Bundle args = new Bundle();
        args.putString(EXTRA_TITLE, title);
        args.putString(EXTRA_TEXT, text);
        args.putString(EXTRA_TASK_ID, taskId);
        args.putString(EXTRA_STATUS, status);
        TaskDialog fragment = new TaskDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public void setListener(ITaskListener listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_offer_candidate, container, false);
        ButterKnife.bind(this, view);
        setCancelable(false);
        titleView.setText(getArguments().getString(EXTRA_TITLE));
        textView.setText(getArguments().getString(EXTRA_TEXT));
        return view;
    }

    @OnClick(R.id.popupOkBtn)
    void onOkClicked(View v) {
        listener.acceptClick(getArguments().getString(EXTRA_TASK_ID), getArguments().getString(EXTRA_STATUS));
        dismiss();
    }

    @OnClick(R.id.popupCancelBtn)
    void onCancelClicked(View v) {
        this.dismiss();
    }

    private void sendResult(int resultCode) {
        if (getTargetFragment() == null)
            return;

        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, null);
    }

    public interface ITaskListener{
        void acceptClick(String taskId, String status);
    }
}
