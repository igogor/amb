package com.kitapp.ambassador.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.kitapp.ambassador.R;
import com.kitapp.ambassador.helpers.Constants;

public class GenderDialog extends AppCompatDialogFragment {

    public static final String EXTRA_GENDER = GenderDialog.class.getName() + "gender";
    private String selection;

    public static GenderDialog newInstance(String gender) {

        Bundle args = new Bundle();
        args.putString(EXTRA_GENDER, gender);
        GenderDialog fragment = new GenderDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final String[] genders = getResources().getStringArray(R.array.gender_array);
        String currentGender = "";
        if (getArguments() != null) {
            currentGender = getArguments().getString(EXTRA_GENDER);
        }

        int checkedIndex = currentGender.equals(Constants.GENDER_MALE) ? 0 : 1;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.dialog_gender_title));
        builder.setSingleChoiceItems(R.array.gender_array, checkedIndex, (dialogInterface, i) -> selection = genders[i]);
        builder.setPositiveButton(android.R.string.ok, (dialogInterface, i) -> sendResult(Activity.RESULT_OK));
        builder.setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.cancel());
        return builder.create();
    }

    private void sendResult(int resultCode) {
        if (getTargetFragment() == null)
            return;

        Intent intent = new Intent();
        intent.putExtra(EXTRA_GENDER, selection);
        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
    }

}
